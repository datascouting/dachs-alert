package com.datascouting.dachs.alert.mapper;

import com.datascouting.dachs.alert.domain.DachsAlertDomainConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan
@Import(DachsAlertDomainConfiguration.class)
public class DachsAlertMapperConfiguration {
}
