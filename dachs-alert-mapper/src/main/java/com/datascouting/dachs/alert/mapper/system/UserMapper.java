package com.datascouting.dachs.alert.mapper.system;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.dto.UserDto;
import com.datascouting.dachs.alert.mapper.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(uses = RoleMapper.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper extends GenericMapper<User, UserDto> {
}
