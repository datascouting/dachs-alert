package com.datascouting.dachs.alert.mapper.system;

import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.dto.RoleDto;
import com.datascouting.dachs.alert.mapper.GenericMapper;
import org.mapstruct.Mapper;

@Mapper
public interface RoleMapper extends GenericMapper<Role, RoleDto> {
}
