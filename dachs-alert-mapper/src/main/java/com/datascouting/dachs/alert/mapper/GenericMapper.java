package com.datascouting.dachs.alert.mapper;

import com.datascouting.dachs.alert.domain.entity.AbstractEntity;
import com.datascouting.dachs.alert.dto.AbstractDto;
import org.intellift.sol.mapper.PageMapper;

public interface GenericMapper<E extends AbstractEntity, D extends AbstractDto> extends PageMapper<E, D> {
}

