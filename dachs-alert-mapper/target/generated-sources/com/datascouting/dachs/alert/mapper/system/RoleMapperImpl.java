package com.datascouting.dachs.alert.mapper.system;

import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.dto.RoleDto;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-30T00:30:39+0200",
    comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class RoleMapperImpl implements RoleMapper {

    @Override
    public RoleDto mapTo(Role arg0) {
        if ( arg0 == null ) {
            return null;
        }

        RoleDto roleDto = new RoleDto();

        roleDto.setId( arg0.getId() );
        roleDto.setCreatedAt( arg0.getCreatedAt() );
        roleDto.setUpdatedAt( arg0.getUpdatedAt() );
        roleDto.setName( arg0.getName() );
        roleDto.setDescription( arg0.getDescription() );

        return roleDto;
    }

    @Override
    public Role mapFrom(RoleDto arg0) {
        if ( arg0 == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( arg0.getId() );
        role.setCreatedAt( arg0.getCreatedAt() );
        role.setUpdatedAt( arg0.getUpdatedAt() );
        role.setName( arg0.getName() );
        role.setDescription( arg0.getDescription() );

        return role;
    }
}
