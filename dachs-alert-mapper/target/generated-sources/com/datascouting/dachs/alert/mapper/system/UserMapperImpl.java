package com.datascouting.dachs.alert.mapper.system;

import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.dto.RoleDto;
import com.datascouting.dachs.alert.dto.UserDto;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2019-10-30T00:30:39+0200",
    comments = "version: 1.0.0.Final, compiler: javac, environment: Java 1.8.0_222 (Private Build)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public UserDto mapTo(User arg0) {
        if ( arg0 == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( arg0.getId() );
        userDto.setCreatedAt( arg0.getCreatedAt() );
        userDto.setUpdatedAt( arg0.getUpdatedAt() );
        userDto.setEmail( arg0.getEmail() );
        userDto.setImageUrl( arg0.getImageUrl() );
        userDto.setFirstName( arg0.getFirstName() );
        userDto.setLastName( arg0.getLastName() );
        userDto.setActiveReports( arg0.getActiveReports() );
        userDto.setActiveMonitoring( arg0.getActiveMonitoring() );
        userDto.setActiveLiveAlerts( arg0.getActiveLiveAlerts() );
        userDto.setTwitterDisplayName( arg0.getTwitterDisplayName() );
        userDto.setHateSpeechThreshold( arg0.getHateSpeechThreshold() );
        userDto.setReportFrequencyCron( arg0.getReportFrequencyCron() );
        userDto.setLastReportAt( arg0.getLastReportAt() );
        userDto.setRoles( roleSetToRoleDtoSet( arg0.getRoles() ) );

        return userDto;
    }

    @Override
    public User mapFrom(UserDto arg0) {
        if ( arg0 == null ) {
            return null;
        }

        User user = new User();

        user.setId( arg0.getId() );
        user.setCreatedAt( arg0.getCreatedAt() );
        user.setUpdatedAt( arg0.getUpdatedAt() );
        user.setEmail( arg0.getEmail() );
        user.setImageUrl( arg0.getImageUrl() );
        user.setFirstName( arg0.getFirstName() );
        user.setLastName( arg0.getLastName() );
        user.setTwitterDisplayName( arg0.getTwitterDisplayName() );
        user.setActiveReports( arg0.getActiveReports() );
        user.setActiveMonitoring( arg0.getActiveMonitoring() );
        user.setActiveLiveAlerts( arg0.getActiveLiveAlerts() );
        user.setHateSpeechThreshold( arg0.getHateSpeechThreshold() );
        user.setReportFrequencyCron( arg0.getReportFrequencyCron() );
        user.setLastReportAt( arg0.getLastReportAt() );
        user.setRoles( roleDtoSetToRoleSet( arg0.getRoles() ) );

        return user;
    }

    protected Set<RoleDto> roleSetToRoleDtoSet(Set<Role> set) {
        if ( set == null ) {
            return null;
        }

        Set<RoleDto> set_ = new HashSet<RoleDto>();
        for ( Role role : set ) {
            set_.add( roleMapper.mapTo( role ) );
        }

        return set_;
    }

    protected Set<Role> roleDtoSetToRoleSet(Set<RoleDto> set) {
        if ( set == null ) {
            return null;
        }

        Set<Role> set_ = new HashSet<Role>();
        for ( RoleDto roleDto : set ) {
            set_.add( roleMapper.mapFrom( roleDto ) );
        }

        return set_;
    }
}
