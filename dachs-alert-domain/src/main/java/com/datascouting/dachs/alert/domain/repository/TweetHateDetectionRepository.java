package com.datascouting.dachs.alert.domain.repository;

import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import org.springframework.stereotype.Repository;

@Repository
public interface TweetHateDetectionRepository extends GenericRepository<TweetHateDetection> {
}
