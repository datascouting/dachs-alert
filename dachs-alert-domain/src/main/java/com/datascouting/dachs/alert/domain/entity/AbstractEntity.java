package com.datascouting.dachs.alert.domain.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.intellift.sol.domain.Identifiable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Identifiable<String>, Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "com.datascouting.dachs.alert.domain.generator.UUIDGenerator")
    protected String id;

    @Column(name = "created_at")
    protected Date createdAt;

    @Column(name = "updated_at")
    protected Date updatedAt;

    @PrePersist
    protected void onPrePersist() {
        createdAt = updatedAt = new Date();
    }

    @PreUpdate
    protected void onPreUpdate() {
        updatedAt = new Date();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id='" + id + '\'' +
                '}';
    }
}
