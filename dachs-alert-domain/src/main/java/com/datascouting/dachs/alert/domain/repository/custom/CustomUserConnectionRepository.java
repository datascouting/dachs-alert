package com.datascouting.dachs.alert.domain.repository.custom;

public interface CustomUserConnectionRepository {

    Integer deleteAllByUserId(String userId);
}
