package com.datascouting.dachs.alert.domain.repository;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Option;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User> {

    Option<User> findById(String id);
}
