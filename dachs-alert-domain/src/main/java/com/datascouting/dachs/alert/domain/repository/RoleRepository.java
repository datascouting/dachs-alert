package com.datascouting.dachs.alert.domain.repository;

import com.datascouting.dachs.alert.domain.entity.Role;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends GenericRepository<Role> {

    Optional<Role> findByName(String name);
}
