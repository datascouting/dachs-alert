package com.datascouting.dachs.alert.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "`user`")
@Entity
public class User extends AbstractEntity {

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column
    private String imageUrl;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column(name = "twitter_display_name", nullable = false)
    private String twitterDisplayName;

    @Column(name = "active_reports")
    private Boolean activeReports;

    @Column(name = "active_monitoring")
    private Boolean activeMonitoring;

    @Column(name = "active_live_alerts")
    private Boolean activeLiveAlerts;

    @Column(name = "hate_speech_threshold")
    private Integer hateSpeechThreshold;

    @Column(name = "report_frequency_cron")
    private String reportFrequencyCron;

    @Column(name = "last_home_timeline_tweet_id")
    private Long lastHomeTimelineTweetId;

    @Column(name = "last_report_at")
    private Date lastReportAt;

    @ManyToMany(fetch = EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;
}
