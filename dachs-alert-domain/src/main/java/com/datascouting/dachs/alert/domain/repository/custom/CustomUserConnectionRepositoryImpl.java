package com.datascouting.dachs.alert.domain.repository.custom;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static java.util.Objects.requireNonNull;

@Repository
@RequiredArgsConstructor
public class CustomUserConnectionRepositoryImpl implements CustomUserConnectionRepository {

    private final EntityManager entityManager;

    @Override
    @Transactional
    public Integer deleteAllByUserId(final String userId) {
        requireNonNull(userId, "userId is null");

        return entityManager
                .createNativeQuery("DELETE FROM userconnection WHERE userid = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }
}
