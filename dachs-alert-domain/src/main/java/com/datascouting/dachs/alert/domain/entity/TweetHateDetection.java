package com.datascouting.dachs.alert.domain.entity;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data
@Builder
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "tweet_hate_detection")
@Entity
public class TweetHateDetection extends AbstractEntity {

    @Column(name = "tweet_id", nullable = false)
    private Long tweetId;

    @Enumerated(EnumType.STRING)
    @Column(name = "hate_detection_status", nullable = false)
    private HateDetectionStatus hateDetectionStatus;

    @Column(name = "hate_speech_rate", nullable = true)
    private Double hateSpeechRate;

    @Column(name = "personal_attack_rate", nullable = true)
    private Double personalAttackRate;

    @Column(name = "retries", nullable = false)
    private Integer retries;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
