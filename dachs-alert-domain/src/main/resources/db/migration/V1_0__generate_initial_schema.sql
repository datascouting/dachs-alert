create table "user" (
  id                          varchar(255) not null constraint user_pk primary key,
  email                       varchar(255) not null,
  password                    varchar(255) not null,
  image_url                   varchar(255),
  first_name                  varchar(255),
  last_name                   varchar(255),
  twitter_display_name        varchar(255) not null,
  active_reports              boolean,
  active_monitoring           boolean,
  hate_speech_threshold       integer,
  report_frequency_cron       varchar(255),
  last_home_timeline_tweet_id bigint default 0
);

create table role (
  id          varchar(255) not null constraint role_pk primary key,
  description varchar(255),
  name        varchar(255) not null
);

create table user_role (
  user_id varchar(255) not null constraint user_role_user_fk references "user",
  role_id varchar(255) not null constraint user_role_role_fk references role,
  constraint user_role_pk primary key (user_id, role_id)
);

INSERT INTO "role" (id, name, description)
values ('d19aa369-0b5a-46a8-bc6a-2ae735277f57', 'JOURNALIST', 'JOURNALIST');

create table "tweet_hate_detection" (
  id                    varchar(255) not null constraint tweet_hate_detection_pk primary key,
  hate_detection_status varchar(255) not null,
  tweet_id              bigint       not null,
  hate_speech_rate      double precision,
  personal_attack_rate  double precision,
  user_id               varchar(255) not null,
  CONSTRAINT fk_tweet_hate_detection_user_id FOREIGN KEY (user_id) REFERENCES "user" (id)
);

-- Spring sql script for social connections
create table UserConnection (
  userId         varchar(255) not null,
  providerId     varchar(255) not null,
  providerUserId varchar(255),
  rank           int          not null,
  displayName    varchar(255),
  profileUrl     varchar(512),
  imageUrl       varchar(512),
  accessToken    varchar(512) not null,
  secret         varchar(512),
  refreshToken   varchar(512),
  expireTime     bigint,
  primary key (userId, providerId, providerUserId)
);

create unique index UserConnectionRank
  on UserConnection (userId, providerId, rank);
