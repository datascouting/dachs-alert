alter table "tweet_hate_detection"
  add column created_at timestamp not null default now(),
  add column updated_at timestamp not null default now();

alter table "user"
  add column created_at timestamp not null default now(),
  add column updated_at timestamp not null default now();

alter table role
  add column created_at timestamp not null default now(),
  add column updated_at timestamp not null default now();
