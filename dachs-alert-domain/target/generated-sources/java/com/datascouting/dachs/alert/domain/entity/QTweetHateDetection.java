package com.datascouting.dachs.alert.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QTweetHateDetection is a Querydsl query type for TweetHateDetection
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QTweetHateDetection extends EntityPathBase<TweetHateDetection> {

    private static final long serialVersionUID = -1878703443L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QTweetHateDetection tweetHateDetection = new QTweetHateDetection("tweetHateDetection");

    public final QAbstractEntity _super = new QAbstractEntity(this);

    //inherited
    public final DateTimePath<java.util.Date> createdAt = _super.createdAt;

    public final EnumPath<com.datascouting.dachs.alert.common.constant.HateDetectionStatus> hateDetectionStatus = createEnum("hateDetectionStatus", com.datascouting.dachs.alert.common.constant.HateDetectionStatus.class);

    public final NumberPath<Double> hateSpeechRate = createNumber("hateSpeechRate", Double.class);

    //inherited
    public final StringPath id = _super.id;

    public final NumberPath<Double> personalAttackRate = createNumber("personalAttackRate", Double.class);

    public final NumberPath<Integer> retries = createNumber("retries", Integer.class);

    public final NumberPath<Long> tweetId = createNumber("tweetId", Long.class);

    //inherited
    public final DateTimePath<java.util.Date> updatedAt = _super.updatedAt;

    public final QUser user;

    public QTweetHateDetection(String variable) {
        this(TweetHateDetection.class, forVariable(variable), INITS);
    }

    public QTweetHateDetection(Path<? extends TweetHateDetection> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QTweetHateDetection(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QTweetHateDetection(PathMetadata metadata, PathInits inits) {
        this(TweetHateDetection.class, metadata, inits);
    }

    public QTweetHateDetection(Class<? extends TweetHateDetection> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.user = inits.isInitialized("user") ? new QUser(forProperty("user")) : null;
    }

}

