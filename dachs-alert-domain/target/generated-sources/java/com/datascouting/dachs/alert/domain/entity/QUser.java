package com.datascouting.dachs.alert.domain.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QUser is a Querydsl query type for User
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QUser extends EntityPathBase<User> {

    private static final long serialVersionUID = -1628957138L;

    public static final QUser user = new QUser("user");

    public final QAbstractEntity _super = new QAbstractEntity(this);

    public final BooleanPath activeLiveAlerts = createBoolean("activeLiveAlerts");

    public final BooleanPath activeMonitoring = createBoolean("activeMonitoring");

    public final BooleanPath activeReports = createBoolean("activeReports");

    //inherited
    public final DateTimePath<java.util.Date> createdAt = _super.createdAt;

    public final StringPath email = createString("email");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Integer> hateSpeechThreshold = createNumber("hateSpeechThreshold", Integer.class);

    //inherited
    public final StringPath id = _super.id;

    public final StringPath imageUrl = createString("imageUrl");

    public final NumberPath<Long> lastHomeTimelineTweetId = createNumber("lastHomeTimelineTweetId", Long.class);

    public final StringPath lastName = createString("lastName");

    public final DateTimePath<java.util.Date> lastReportAt = createDateTime("lastReportAt", java.util.Date.class);

    public final StringPath password = createString("password");

    public final StringPath reportFrequencyCron = createString("reportFrequencyCron");

    public final SetPath<Role, QRole> roles = this.<Role, QRole>createSet("roles", Role.class, QRole.class, PathInits.DIRECT2);

    public final StringPath twitterDisplayName = createString("twitterDisplayName");

    //inherited
    public final DateTimePath<java.util.Date> updatedAt = _super.updatedAt;

    public QUser(String variable) {
        super(User.class, forVariable(variable));
    }

    public QUser(Path<? extends User> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUser(PathMetadata metadata) {
        super(User.class, metadata);
    }

}

