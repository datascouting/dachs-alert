ALTER TABLE "user"
  add column last_report_at timestamp not null default now();
