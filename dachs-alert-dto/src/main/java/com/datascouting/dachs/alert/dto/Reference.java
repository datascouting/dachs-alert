package com.datascouting.dachs.alert.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class Reference extends AbstractDto {

    public Reference() {
    }

    public Reference(String id) {
        this.id = id;
    }
}
