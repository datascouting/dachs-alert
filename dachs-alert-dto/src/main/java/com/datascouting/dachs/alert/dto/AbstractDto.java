package com.datascouting.dachs.alert.dto;

import lombok.Data;
import org.intellift.sol.domain.Identifiable;

import java.util.Date;

@Data
public abstract class AbstractDto implements Identifiable<String> {

    protected String id;

    protected Date createdAt;

    protected Date updatedAt;
}
