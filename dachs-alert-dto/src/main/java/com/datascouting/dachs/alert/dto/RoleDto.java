package com.datascouting.dachs.alert.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RoleDto extends AbstractDto {

    private String name;

    private String description;
}
