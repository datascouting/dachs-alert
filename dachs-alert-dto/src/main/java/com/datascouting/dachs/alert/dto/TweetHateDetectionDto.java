package com.datascouting.dachs.alert.dto;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class TweetHateDetectionDto extends AbstractDto {

    private Long tweetId;

    private HateDetectionStatus hateDetectionStatus;

    private Double hateSpeechRate;

    private Double personalAttachRate;

    private Integer retries;

    private Reference user;
}
