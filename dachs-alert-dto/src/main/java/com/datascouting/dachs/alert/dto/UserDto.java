package com.datascouting.dachs.alert.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserDto extends AbstractDto {

    private String email;

    private String imageUrl;

    private String firstName;

    private String lastName;

    private Boolean activeReports;

    private Boolean activeMonitoring;

    private Boolean activeLiveAlerts;

    private String twitterDisplayName;

    private Integer hateSpeechThreshold;

    private String reportFrequencyCron;

    private Long lastHomeTimelineTwitterId;

    private Date lastReportAt;

    private Set<RoleDto> roles;
}
