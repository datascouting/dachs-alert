package com.datascouting.dachs.alert.sdk.client;

import com.datascouting.dachs.alert.dto.AbstractDto;
import org.intellift.sol.sdk.client.CrudApiClient;

public interface CrudClient<D extends AbstractDto> extends CrudApiClient<D, String> {
}
