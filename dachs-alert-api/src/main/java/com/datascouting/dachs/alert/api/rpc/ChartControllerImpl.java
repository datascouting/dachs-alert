package com.datascouting.dachs.alert.api.rpc;

import com.datascouting.dachs.alert.common.constant.DachsAlertApiEndpoints;
import com.datascouting.dachs.alert.security.model.SecurityUser;
import com.datascouting.dachs.alert.service.chart.charts.conversation.health.ConversationHealthChartService;
import com.datascouting.dachs.alert.service.chart.charts.conversation.health.ConversationHealthResponse;
import com.datascouting.dachs.alert.service.chart.charts.trend.TrendChartService;
import com.datascouting.dachs.alert.service.chart.charts.trend.TrendResponse;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Objects;

@RequestMapping(DachsAlertApiEndpoints.RPC_CHARTS)
@RestController
@RequiredArgsConstructor
public class ChartControllerImpl implements ChartController {

    private final UserService userService;
    private final ConversationHealthChartService metricsChartService;
    private final TrendChartService trendChartService;
    private final DachsAlertConfigurationProperties properties;

    @Override
    @GetMapping("/conversation-health")
    public ResponseEntity<ConversationHealthResponse> getConversationHealth(Authentication authentication,
                                                                            @RequestParam final Long startDate,
                                                                            @RequestParam final Long endDate) {
        Objects.requireNonNull(startDate, "startDate is null");
        Objects.requireNonNull(endDate, "endDate is null");

        return Try.of(() -> (SecurityUser) authentication.getPrincipal())
                .flatMap(securityUser -> userService.findOne(securityUser.getId()))
                .flatMap(userOption -> userOption.toTry(() -> new RuntimeException("User not found exception")))
                .flatMap(user -> metricsChartService.getConversationHealth(user, new Date(startDate), new Date(endDate)))
                .map(ResponseEntity::ok)
                .getOrElseThrow(() -> new RuntimeException("Could not generate metrics chart"));
    }

    @Override
    @GetMapping("/trend")
    public ResponseEntity<TrendResponse> getTrends(Authentication authentication,
                                                   @RequestParam final Long startDate,
                                                   @RequestParam final Long endDate) {
        Objects.requireNonNull(startDate, "startDate is null");
        Objects.requireNonNull(endDate, "endDate is null");

        return Try.of(() -> (SecurityUser) authentication.getPrincipal())
                .flatMap(securityUser -> userService.findOne(securityUser.getId()))
                .flatMap(userOption -> userOption.toTry(() -> new RuntimeException("User not found exception")))
                .flatMap(user -> trendChartService.getTrend(user, new Date(startDate), new Date(endDate), properties.getChart().getTrendGrouping()))
                .map(ResponseEntity::ok)
                .getOrElseThrow(() -> new RuntimeException("Could not generate trend chart"));
    }
}
