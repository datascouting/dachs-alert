package com.datascouting.dachs.alert.api.configuration.security.social;

import com.datascouting.dachs.alert.security.model.SecurityUser;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ProviderSignInController;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
public class SocialConfig implements SocialConfigurer {

    private final DataSource dataSource;
    private final TwitterSignInAdapter twitterSignInAdapter;
    private final TwitterConnectionSignUp twitterConnectionSignUp;
    private final DachsAlertConfigurationProperties properties;

    @Bean
    public ConnectionFactoryLocator connectionFactoryLocator() {
        return new ConnectionFactoryRegistry();
    }

    @Bean
    public ProviderSignInController providerSignInController(
            final ConnectionFactoryLocator connectionFactoryLocator,
            final UsersConnectionRepository usersConnectionRepository) {
        final ProviderSignInController providerSignInController = new ProviderSignInController(
                connectionFactoryLocator,
                usersConnectionRepository,
                twitterSignInAdapter
        );

        providerSignInController.setApplicationUrl(properties.getApplicationUrl());

        return providerSignInController;
    }

    @Override
    public void addConnectionFactories(
            final ConnectionFactoryConfigurer connectionFactoryConfigurer,
            final Environment env) {
    }

    @Override
    public UserIdSource getUserIdSource() {
        return () -> {
            final Authentication authentication = SecurityContextHolder
                    .getContext()
                    .getAuthentication();

            if (authentication == null) {
                throw new IllegalStateException("Unable to get a ConnectionRepository: no user signed in");
            }

            final SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();

            return securityUser.getId();
        };
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(final ConnectionFactoryLocator connectionFactoryLocator) {
        final JdbcUsersConnectionRepository usersConnectionRepository = new JdbcUsersConnectionRepository(
                dataSource,
                connectionFactoryLocator(),
                Encryptors.noOpText()
        );

        usersConnectionRepository.setConnectionSignUp(twitterConnectionSignUp);

        return usersConnectionRepository;
    }
}
