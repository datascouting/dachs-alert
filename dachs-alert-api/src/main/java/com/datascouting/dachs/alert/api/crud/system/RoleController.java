package com.datascouting.dachs.alert.api.crud.system;

import com.datascouting.dachs.alert.api.crud.GenericCrudController;
import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.dto.RoleDto;

public interface RoleController extends GenericCrudController<Role, RoleDto, RoleDto> {
}
