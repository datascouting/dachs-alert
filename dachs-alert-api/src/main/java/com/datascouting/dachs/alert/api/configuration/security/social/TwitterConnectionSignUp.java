package com.datascouting.dachs.alert.api.configuration.security.social;

import com.datascouting.dachs.alert.domain.entity.AbstractEntity;
import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.crud.system.RoleService;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.datascouting.dachs.alert.service.util.UrlUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Date;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TwitterConnectionSignUp implements ConnectionSignUp {

    private final UserService userService;
    private final RoleService roleService;
    private final Environment environment;

    @Override
    public String execute(final Connection<?> connection) {
        Objects.requireNonNull(connection, "connection is null");

        return createNewUserInstance(connection)
                .flatMap(userService::save)
                .map(AbstractEntity::getId)
                .getOrElse(() -> null);
    }

    private Try<User> createNewUserInstance(final Connection<?> connection) {
        Objects.requireNonNull(connection, "connection is null");

        return getUserEmail(connection)
                .flatMap(email -> UrlUtil.removeProtocol(connection.getImageUrl())
                        .flatMap(imageUrl -> Try.of(() -> {
                            final Role journalistRole = roleService.getJournalistRole();

                            final UserProfile userProfile = connection.fetchUserProfile();

                            final User user = new User();
                            user.setEmail(email);
                            user.setPassword(connection.getKey().getProviderId() + ":" + connection.getKey().getProviderUserId());
                            user.setImageUrl(imageUrl);
                            user.setFirstName(userProfile.getFirstName());
                            user.setLastName(userProfile.getLastName());
                            user.setTwitterDisplayName(connection.getDisplayName());
                            user.setLastReportAt(new Date());
                            user.setRoles(Collections.singleton(journalistRole));

                            return user;
                        })));
    }

    private Try<String> getUserEmail(final Connection<?> connection) {
        Objects.requireNonNull(connection, "connection is null");

        return Try.of(() -> {
            final TwitterTemplate twitterTemplate = new TwitterTemplate(
                    environment.getProperty("spring.social.twitter.appId"),
                    environment.getProperty("spring.social.twitter.appSecret"),
                    connection.createData().getAccessToken(),
                    connection.createData().getSecret());

            final RestTemplate restTemplate = twitterTemplate.getRestTemplate();
            final String response = restTemplate.getForObject("https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true", String.class);
            final ObjectNode node = new ObjectMapper().readValue(response, ObjectNode.class);

            if (node.has("email")) {
                final JsonNode email = node.get("email");
                return email.asText();
            } else {
                return connection.getDisplayName();
            }
        });
    }
}
