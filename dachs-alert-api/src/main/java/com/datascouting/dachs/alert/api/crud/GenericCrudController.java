package com.datascouting.dachs.alert.api.crud;

import com.datascouting.dachs.alert.domain.entity.AbstractEntity;
import com.datascouting.dachs.alert.dto.AbstractDto;
import org.intellift.sol.controller.querydsl.api.QueryDslAsymmetricCrudApiController;

public interface GenericCrudController<E extends AbstractEntity, D extends AbstractDto, RD extends AbstractDto> extends QueryDslAsymmetricCrudApiController<E, D, RD, String> {
}
