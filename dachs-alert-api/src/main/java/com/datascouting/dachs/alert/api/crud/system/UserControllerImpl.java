package com.datascouting.dachs.alert.api.crud.system;

import com.datascouting.dachs.alert.common.constant.DachsAlertApiEndpoints;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.dto.UserDto;
import com.datascouting.dachs.alert.mapper.system.UserMapper;
import com.datascouting.dachs.alert.security.model.SecurityUser;
import com.datascouting.dachs.alert.service.alert.report.schedule.CronScheduler;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.querydsl.core.types.Predicate;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.intellift.sol.mapper.PageMapper;
import org.intellift.sol.service.querydsl.QueryDslCrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(DachsAlertApiEndpoints.USERS)
@RequiredArgsConstructor
public class UserControllerImpl implements UserController {

    private final UserService userService;
    private final UserMapper userMapper;
    private final CronScheduler cronScheduler;

    @Override
    public PageMapper<User, UserDto> getMapper() {
        return userMapper;
    }

    @Override
    public PageMapper<User, UserDto> getReferenceMapper() {
        return userMapper;
    }

    @Override
    public QueryDslCrudService<User, String> getService() {
        return userService;
    }

    @Override
    @PutMapping({"/{id}"})
    public ResponseEntity<UserDto> put(@PathVariable("id") String id, @RequestBody UserDto dto) {
        return Try
                .of(() -> {
                    final User entity = userMapper.mapFrom(dto);
                    entity.setId(id);
                    return entity;
                })
                .flatMap(entity -> userService.exists(entity.getId())
                        .flatMap(doesExist -> doesExist

                                ? userService.update(entity)
                                .flatMap(user -> cronScheduler.schedule(user)
                                        .map(aVoid -> user))
                                .map(userMapper::mapTo)
                                .map(ResponseEntity::ok)

                                : Try.failure(new RuntimeException("Could not update user"))))
                .onFailure((throwable) -> getLogger().error("Error occurred while processing PUT request", throwable))
                .getOrElse(() -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
    }

    @Override
    public ResponseEntity<UserDto> post(UserDto dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResponseEntity<Void> delete(String s) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ResponseEntity<Page<UserDto>> getAll(@QuerydslPredicate(root = User.class) Predicate predicate, Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }

    @Override
    @GetMapping({"/me"})
    public ResponseEntity<UserDto> me(Authentication authentication) {
        final SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();

        return userService.findOne(securityUser.getId())
                .flatMap(userOption -> userOption.toTry(() -> new RuntimeException("User not found exception")))
                .map(userMapper::mapTo)
                .map(ResponseEntity::ok)
                .getOrElseThrow(() -> new RuntimeException("Could not respond with user info"));
    }
}
