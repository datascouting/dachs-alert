package com.datascouting.dachs.alert.api.configuration.security.social;

import com.datascouting.dachs.alert.security.jwt.JwtTokenProvider;
import com.datascouting.dachs.alert.security.model.SecurityUser;
import com.datascouting.dachs.alert.security.service.SecurityUserDetailsService;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

@Service
@RequiredArgsConstructor
public class TwitterSignInAdapter implements SignInAdapter {

    private final SecurityUserDetailsService securityUserDetailsService;
    private final JwtTokenProvider jwtTokenProvider;
    private final DachsAlertConfigurationProperties dachsAlertConfigurationProperties;

    @Override
    public String signIn(final String localUserId,
                         final Connection<?> connection,
                         final NativeWebRequest webRequest) {
        final SecurityUser securityUser = securityUserDetailsService.loadUserByUsername(localUserId);
        final UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        securityUser,
                        securityUser.getPassword(),
                        securityUser.getAuthorities()
                );

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        final String token = jwtTokenProvider.createToken(
                securityUser.getId(),
                securityUser.getRoles()
        );

        return dachsAlertConfigurationProperties.getPostSignInUrl() + "/" + token;
    }
}
