package com.datascouting.dachs.alert.api.configuration.security;

import com.datascouting.dachs.alert.api.configuration.security.jwt.JwtConfigurer;
import com.datascouting.dachs.alert.security.jwt.JwtTokenProvider;
import com.datascouting.dachs.alert.security.service.SecurityUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletResponse;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final SecurityUserDetailsService securityUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public void configure(final AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(securityUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.csrf()
                .disable()

                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()

                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED))

                .and()

                .authorizeRequests()

                .antMatchers(publicAntMatchers())
                .permitAll()

                .anyRequest()
                .authenticated()

                .and()

                .cors()

                .and()

                .apply(new JwtConfigurer(jwtTokenProvider));
    }

    private String[] publicAntMatchers() {
        return new String[]{
                "/favicon.ico",
                "/robots.txt",
                "/connect/**",
                "/signin/**",
                "/signup/**",
                "/login"
        };
    }
}
