package com.datascouting.dachs.alert.api;

import com.datascouting.dachs.alert.domain.DachsAlertDomainConfiguration;
import com.datascouting.dachs.alert.mapper.DachsAlertMapperConfiguration;
import com.datascouting.dachs.alert.security.DachsAlertSecurityConfiguration;
import com.datascouting.dachs.alert.service.DachsAlertServiceConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.intellift.sol.controller.SolControllerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.social.config.annotation.EnableSocial;

@Slf4j
@SpringBootApplication
@EnableSocial
@Import({
        SolControllerConfiguration.class,
        DachsAlertDomainConfiguration.class,
        DachsAlertMapperConfiguration.class,
        DachsAlertServiceConfiguration.class,
        DachsAlertSecurityConfiguration.class,
})
public class DachsAlertApplication {

    public static void main(final String... args) {
        SpringApplication.run(DachsAlertApplication.class, args);
    }
}
