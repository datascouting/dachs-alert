package com.datascouting.dachs.alert.api.rpc;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

public interface OptOutController {

    ResponseEntity<Void> optOut(Authentication authentication);
}
