package com.datascouting.dachs.alert.api.rpc;

import com.datascouting.dachs.alert.common.constant.DachsAlertApiEndpoints;
import com.datascouting.dachs.alert.security.model.SecurityUser;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.datascouting.dachs.alert.service.opt.out.OptOutService;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(DachsAlertApiEndpoints.RPC_OPT_OUT)
@RestController
@RequiredArgsConstructor
public class OptOutControllerImpl implements OptOutController {

    private final UserService userService;
    private final OptOutService optOutService;

    @Override
    @PostMapping
    public ResponseEntity<Void> optOut(final Authentication authentication) {
        return Try.of(() -> (SecurityUser) authentication.getPrincipal())
                .flatMap(securityUser -> userService.findOne(securityUser.getId()))
                .flatMap(userOption -> userOption.toTry(() -> new RuntimeException("User not found exception")))
                .flatMap(optOutService::optOut)
                .map(ResponseEntity::ok)
                .getOrElseThrow(() -> new RuntimeException("Could not opt out user"));
    }
}
