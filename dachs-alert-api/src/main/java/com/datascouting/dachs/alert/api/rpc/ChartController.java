package com.datascouting.dachs.alert.api.rpc;

import com.datascouting.dachs.alert.service.chart.charts.conversation.health.ConversationHealthResponse;
import com.datascouting.dachs.alert.service.chart.charts.trend.TrendResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

public interface ChartController {

    ResponseEntity<ConversationHealthResponse> getConversationHealth(Authentication authentication, Long startDate, Long endDate);

    ResponseEntity<TrendResponse> getTrends(Authentication authentication, Long startDate, Long endDate);
}
