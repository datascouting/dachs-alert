package com.datascouting.dachs.alert.api.crud.system;

import com.datascouting.dachs.alert.common.constant.DachsAlertApiEndpoints;
import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.dto.RoleDto;
import com.datascouting.dachs.alert.mapper.system.RoleMapper;
import com.datascouting.dachs.alert.service.crud.system.RoleService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.intellift.sol.mapper.PageMapper;
import org.intellift.sol.service.querydsl.QueryDslCrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(DachsAlertApiEndpoints.ROLES)
@RestController
@RequiredArgsConstructor
public class RoleControllerImpl implements RoleController {

    private final RoleService roleService;

    private final RoleMapper roleMapper;

    @Override
    public PageMapper<Role, RoleDto> getMapper() {
        return roleMapper;
    }

    @Override
    public PageMapper<Role, RoleDto> getReferenceMapper() {
        return roleMapper;
    }

    @Override
    public QueryDslCrudService<Role, String> getService() {
        return roleService;
    }

    @Override
    public ResponseEntity<Page<RoleDto>> getAll(@QuerydslPredicate(root = Role.class) Predicate predicate, Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
