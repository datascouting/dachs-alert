package com.datascouting.dachs.alert.api.crud.system;

import com.datascouting.dachs.alert.api.crud.GenericCrudController;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.dto.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

import java.security.Principal;

public interface UserController extends GenericCrudController<User, UserDto, UserDto> {

    ResponseEntity<UserDto> me(Authentication authentication);
}
