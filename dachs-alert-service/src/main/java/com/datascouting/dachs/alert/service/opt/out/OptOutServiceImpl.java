package com.datascouting.dachs.alert.service.opt.out;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.crud.custom.UserConnectionService;
import com.datascouting.dachs.alert.service.crud.system.TweetHateDetectionService;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class OptOutServiceImpl implements OptOutService {

    private final TweetHateDetectionService tweetHateDetectionService;
    private final UserConnectionService userConnectionService;
    private final UserService userService;

    @Override
    public Try<Void> optOut(final User user) {
        Objects.requireNonNull(user, "user is null");

        log.info(String.format(
                "Opt out started for user %s %s with id %s",
                user.getFirstName(),
                user.getLastName(),
                user.getId()
        ));

        return tweetHateDetectionService.findAllByUser(user)
                .peek(tweetHateDetections -> log.info(String.format("Deleting %d tweetHateDetections", tweetHateDetections.size())))
                .flatMap(tweetHateDetectionService::delete)
                .flatMap(ignored -> userConnectionService.deleteAllByUser(user))
                .peek(totalDeleted -> log.info(String.format("Deleting %d userConnections", totalDeleted)))
                .flatMap(ignored -> userService.delete(user))
                .peek(aVoid -> log.info("Successful opt out"));
    }
}
