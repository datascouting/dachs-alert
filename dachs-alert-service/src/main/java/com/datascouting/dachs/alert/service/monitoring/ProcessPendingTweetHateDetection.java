package com.datascouting.dachs.alert.service.monitoring;

public interface ProcessPendingTweetHateDetection {

    void process();
}
