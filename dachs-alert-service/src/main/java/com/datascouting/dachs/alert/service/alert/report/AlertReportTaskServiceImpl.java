package com.datascouting.dachs.alert.service.alert.report;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.chart.server.rendering.ChartServerRenderingService;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import com.datascouting.dachs.alert.service.cron.CronEvaluation;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.datascouting.dachs.alert.service.mail.action.AlertReportMailAction;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class AlertReportTaskServiceImpl implements AlertReportTaskService {

    private final ChartServerRenderingService chartServerRenderingService;
    private final AlertReportMailAction alertReportMailAction;
    private final UserService userService;
    private final DachsAlertConfigurationProperties properties;

    @Override
    public Runnable generateTask(final User user) {
        requireNonNull(user, "user is null");

        return () -> {
            log.info("<<--------------------------->>");
            log.info("Start executing alert for user: " + user.getId() + ", " + user.getFirstName() + user.getLastName());

            final List<Date> dates = CronEvaluation.getPrev(user.getLastReportAt(), user.getReportFrequencyCron(), 2);

            if (dates.size() == 2) {
                final Date from = dates.get(0);
                final Date to = dates.get(1);

                chartServerRenderingService.createTrendChartImage(user, from, to, properties.getChart().getTrendGrouping())
                        .flatMap(path -> Try.of(path::toFile))
                        .flatMap(trendImageFile -> chartServerRenderingService.createConversationHealthChartImage(user, from, to)
                                .flatMap(path -> Try.of(path::toFile))
                                .flatMap(conversationHealthImageFile -> alertReportMailAction.send(
                                        user,
                                        from,
                                        to,
                                        conversationHealthImageFile,
                                        trendImageFile,
                                        Locale.ENGLISH
                                )))
                        .flatMap(aVoid -> userService.updateLastReportAt(user.getId()))
                        .onSuccess(aVoid -> {
                            log.info("Finish executing alert for user: " + user.getId() + ", " + user.getFirstName() + user.getLastName());
                            log.info("<<--------------------------->>");
                        })
                        .onFailure(throwable -> log.error("Could not send alert report.", throwable));
            } else {
                log.warn("Could not send alert report. Dates size: " + dates.size());
                log.info("<<--------------------------->>");
            }
        };
    }
}
