package com.datascouting.dachs.alert.service.chart.charts.trend;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import com.datascouting.dachs.alert.domain.entity.QTweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.crud.system.TweetHateDetectionService;
import com.datascouting.dachs.alert.service.util.NumberUtil;
import com.querydsl.core.types.Predicate;
import javaslang.Tuple;
import javaslang.collection.List;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

@Service
@AllArgsConstructor
public class TrendChartServiceImpl implements TrendChartService {

    private final TweetHateDetectionService tweetHateDetectionService;

    @Override
    public Try<TrendResponse> getTrend(final User user,
                                       final Date startDate,
                                       final Date endDate,
                                       final Integer intervalMinutes) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");
        requireNonNull(intervalMinutes, "intervalMinutes is null");

        return createPredicate(user, startDate, endDate)
                .flatMap(predicate -> tweetHateDetectionService.findAll(
                        predicate,
                        new Sort(Sort.Direction.ASC, "createdAt"
                        )))
                .flatMap(tweetHateDetections -> createTrendData(tweetHateDetections, intervalMinutes));
    }

    private Try<Predicate> createPredicate(final User user,
                                           final Date startDate,
                                           final Date endDate) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");

        return Try.of(() -> {
            final QTweetHateDetection tweetHateDetection = QTweetHateDetection.tweetHateDetection;

            return tweetHateDetection.createdAt.between(startDate, endDate)
                    .and(tweetHateDetection.hateDetectionStatus.eq(HateDetectionStatus.COMPLETED))
                    .and(tweetHateDetection.user.id.eq(user.getId()));
        });
    }

    private Try<TrendResponse> createTrendData(final List<TweetHateDetection> tweetHateDetections, final Integer intervalMinutes) {
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");
        Objects.requireNonNull(intervalMinutes, "intervalMinutes is null");

        return Try.of(() -> tweetHateDetections.groupBy(groupedBy(intervalMinutes))
                .map(tweetHateDetectionGroup -> Tuple.of(
                        tweetHateDetectionGroup._1,
                        NumberUtil.sum(tweetHateDetectionGroup._2.map(TweetHateDetection::getHateSpeechRate)),
                        NumberUtil.sum(tweetHateDetectionGroup._2.map(TweetHateDetection::getPersonalAttackRate))
                ))
                .foldLeft(List.<TrendResponse.TrendRow>empty(), (objects, hateSpeechPersonalAttack) -> {
                    final TrendResponse.TrendRow lastRow = objects.lastOption()
                            .getOrElse(() -> TrendResponse.TrendRow.builder()
                                    .hateSpeech(0D)
                                    .hateSpeechSum(0D)
                                    .personalAttack(0D)
                                    .personalAttackSum(0D)
                                    .build());

                    final Long group = hateSpeechPersonalAttack._1;
                    final Double hateSpeech = hateSpeechPersonalAttack._2;
                    final Double personalAttack = hateSpeechPersonalAttack._3;

                    return objects.append(TrendResponse.TrendRow.builder()
                            .id(group)
                            .hateSpeechSum(hateSpeech)
                            .hateSpeech((hateSpeech - lastRow.getHateSpeechSum()) * 100)
                            .personalAttackSum(personalAttack)
                            .personalAttack((personalAttack - lastRow.getPersonalAttackSum()) * 100)
                            .build());
                })
                .transform(trendRows -> TrendResponse.builder()
                        .data(trendRows
                                .map(this::processMetricRow)
                                .sortBy(TrendResponse.TrendRow::getId)
                                .toJavaList())
                        .build()));
    }

    private TrendResponse.TrendRow processMetricRow(final TrendResponse.TrendRow trendRow) {
        requireNonNull(trendRow, "trendRow is null");

        Try.of(() -> String.format("%1$,.2f", trendRow.getHateSpeech()))
                .flatMap(string -> Try.of(() -> Double.valueOf(string)))
                .forEach(trendRow::setHateSpeech);

        Try.of(() -> String.format("%1$,.2f", trendRow.getPersonalAttack()))
                .flatMap(string -> Try.of(() -> Double.valueOf(string)))
                .forEach(trendRow::setPersonalAttack);

        return trendRow;
    }

    private Function<TweetHateDetection, Long> groupedBy(final Integer intervalMinutes) {
        Objects.requireNonNull(intervalMinutes, "intervalMinutes is null");

        return (TweetHateDetection tweetHateDetection) ->
                Option.of(tweetHateDetection.getCreatedAt())
                        .map(Date::getTime)
                        .map(millis -> millis / 1000 / 60)
                        .map(minutes -> minutes / intervalMinutes)
                        .map(Math::ceil)
                        .map(Double::longValue)
                        .map(aLong -> aLong * intervalMinutes * 1000 * 60)
                        .getOrElseThrow(() -> new RuntimeException("Count not group by date"));
    }
}
