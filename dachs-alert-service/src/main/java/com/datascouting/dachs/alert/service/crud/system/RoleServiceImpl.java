package com.datascouting.dachs.alert.service.crud.system;

import com.datascouting.dachs.alert.common.constant.RoleNames;
import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.domain.repository.RoleRepository;
import javaslang.control.Option;
import lombok.RequiredArgsConstructor;
import org.intellift.sol.domain.querydsl.repository.QueryDslRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Override
    public QueryDslRepository<Role, String> getRepository() {
        return roleRepository;
    }

    @Override
    public Role getJournalistRole() {
        return Option.ofOptional(roleRepository.findByName(RoleNames.JOURNALIST))
                .getOrElseThrow(() -> new RuntimeException("Journalist role does not exist. Maybe the database is not properly initialized"));
    }
}
