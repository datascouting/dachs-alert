package com.datascouting.dachs.alert.service.crud.system;

import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.service.crud.GenericCrudService;

public interface RoleService extends GenericCrudService<Role> {

    Role getJournalistRole();
}
