package com.datascouting.dachs.alert.service.cron;

import org.springframework.scheduling.support.CronTrigger;
import org.springframework.scheduling.support.SimpleTriggerContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.util.Objects.requireNonNull;

public abstract class CronEvaluation {

    public static List<Date> getNext(final String cron, final int count) {
        requireNonNull(cron, "cron is null");

        Date nextFireAt = new Date();
        final List<Date> evaluations = new ArrayList<>();

        while (evaluations.size() < count) {
            nextFireAt = getNextTriggerDate(cron, nextFireAt);
            evaluations.add(nextFireAt);
        }

        return evaluations;
    }

    public static List<Date> getPrev(final Date searchStartDate, final String cron, final int count) {
        requireNonNull(searchStartDate, "searchStartDate is null");
        requireNonNull(cron, "cron is null");

        Date nextFireAt = searchStartDate;
        Date now = new Date();

        final List<Date> evaluations = new ArrayList<>();

        while (nextFireAt.before(now)) {
            evaluations.add(nextFireAt);
            nextFireAt = getNextTriggerDate(cron, nextFireAt);
        }

        return evaluations.subList(Math.max(evaluations.size() - count, 0), evaluations.size());
    }

    private static Date getNextTriggerDate(final String cron, final Date nextFireAt) {
        final CronTrigger trigger = new CronTrigger(cron);
        final SimpleTriggerContext triggerContext = new SimpleTriggerContext();
        triggerContext.update(null, null, nextFireAt);
        return trigger.nextExecutionTime(triggerContext);
    }
}
