package com.datascouting.dachs.alert.service.crud.system;

import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.crud.GenericCrudService;
import com.querydsl.core.types.Predicate;
import javaslang.collection.List;
import javaslang.control.Try;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;


public interface TweetHateDetectionService extends GenericCrudService<TweetHateDetection> {

    Try<List<TweetHateDetection>> findAll(Predicate predicate);

    Try<List<TweetHateDetection>> findAll(Predicate predicate, Sort sort);

    Try<List<TweetHateDetection>> findByRetriesLessThanAndPending(Integer monitorMaxRetries, Pageable pageable);

    Try<List<TweetHateDetection>> findAllByUser(User user);
}
