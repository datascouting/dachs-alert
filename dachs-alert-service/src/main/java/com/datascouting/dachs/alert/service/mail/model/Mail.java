package com.datascouting.dachs.alert.service.mail.model;

import javaslang.Tuple2;
import javaslang.collection.List;
import javaslang.control.Option;
import lombok.Builder;
import lombok.Data;

import java.io.File;
import java.util.Set;

@Data
@Builder
public class Mail {

    private String subject;
    private String from;
    private Set<String> to;
    private Set<String> cc;
    private Set<String> bcc;
    private String body;
    private List<Tuple2<String, File>> attachments;

    @Override
    public String toString() {
        return String.format("{subject: %s, from: %s, to: %s, cc: %s, bcc: %s, body: %s}",
                Option.of(subject).getOrElse(""),
                Option.of(from).getOrElse(""),
                Option.of(to).map(strings -> String.join(",", strings)).getOrElse(""),
                Option.of(cc).map(strings -> String.join(",", strings)).getOrElse(""),
                Option.of(bcc).map(strings -> String.join(",", strings)).getOrElse(""),
                Option.of(body).getOrElse("")
        );
    }
}
