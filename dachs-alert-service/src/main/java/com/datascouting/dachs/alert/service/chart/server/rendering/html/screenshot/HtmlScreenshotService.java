package com.datascouting.dachs.alert.service.chart.server.rendering.html.screenshot;

import javaslang.control.Try;

import java.io.File;
import java.nio.file.Path;

public interface HtmlScreenshotService {

    Try<File> screenshot(Path htmlPath, Path destinationPath, Integer viewportWidth, Integer viewportHeight);
}
