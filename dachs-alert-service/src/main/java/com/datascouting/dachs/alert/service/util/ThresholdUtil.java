package com.datascouting.dachs.alert.service.util;

import javaslang.control.Try;

import java.util.Objects;

public class ThresholdUtil {

    public static final Double MAX_THRESHOLD = 1.0;
    public static final Double LOW_THRESHOLD = 0.50;
    public static final Double MEDIUM_THRESHOLD = 0.65;
    public static final Double HIGH_THRESHOLD = 0.75;
    public static final Double VERY_HIGH_THRESHOLD = 0.90;

    public static final int LOW_CODE = 0;
    public static final int MEDIUM_CODE = 1;
    public static final int HIGH_CODE = 2;
    public static final int VERY_HIGH_CODE = 3;

    public static Try<Double> translateToRate(Integer storedThreshold) {
        switch (storedThreshold) {
            case LOW_CODE:
                return Try.success(LOW_THRESHOLD);
            case MEDIUM_CODE:
                return Try.success(MEDIUM_THRESHOLD);
            case HIGH_CODE:
                return Try.success(HIGH_THRESHOLD);
            case VERY_HIGH_CODE:
                return Try.success(VERY_HIGH_THRESHOLD);
            default:
                return Try.failure(new RuntimeException("Could not translateToRate threshold"));
        }
    }

    public static Try<String> translateToText(final Double storedThreshold) {
        Objects.requireNonNull(storedThreshold, "storedThreshold is null");

        if (storedThreshold >= VERY_HIGH_THRESHOLD) {
            return Try.success("very-high");
        } else if (storedThreshold >= HIGH_THRESHOLD) {
            return Try.success("high");
        } else if (storedThreshold >= MEDIUM_THRESHOLD) {
            return Try.success("medium");
        } else {
            return Try.success("low");
        }
    }
}
