package com.datascouting.dachs.alert.service.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Objects.requireNonNull;

public class DateFormatUtil {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public static String format(final Date date) {
        requireNonNull(date, "date is null");

        return dateFormat.format(date);
    }
}
