package com.datascouting.dachs.alert.service.alert.live.report.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class LiveDetection {

    private Long tweetId;
    private String personalAttack;
    private String hateSpeech;
    private Date createdAt;
}
