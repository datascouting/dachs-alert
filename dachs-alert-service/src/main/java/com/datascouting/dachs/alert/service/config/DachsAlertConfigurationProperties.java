package com.datascouting.dachs.alert.service.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@ConfigurationProperties(prefix = "dachs")
@Configuration
@Data
public class DachsAlertConfigurationProperties {

    private String applicationUrl;
    private String postSignInUrl;
    private Long minCronDurationMinutes;

    private Integer monitorMaxRetries;

    private String monitorTwitterTimelinesCron;
    private String processPendingTweetHateDetectionCron;
    private Long liveAlertInterval;

    private Path storagePath;
    private String htmlScreenshotCommand;

    private JwtConfigProperties jwt = new JwtConfigProperties();

    private ChartConfigProperties chart = new ChartConfigProperties();

    @Data
    public static class JwtConfigProperties {
        private String secretKey;
        private Long expireLength;
    }

    @Data
    public static class ChartConfigProperties {
        private Integer trendGrouping;
        private Integer conversationHealthGrouping;
    }
}
