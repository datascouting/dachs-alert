package com.datascouting.dachs.alert.service.crud.custom;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.domain.repository.custom.CustomUserConnectionRepository;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static java.util.Objects.*;
import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class UserConnectionServiceImpl implements UserConnectionService {

    private final UsersConnectionRepository usersConnectionRepository;
    private final CustomUserConnectionRepository customUserConnectionRepository;

    @Override
    public Try<Connection<Twitter>> getTwitterConnection(final User user) {
        requireNonNull(user, "user is null");

        return Try.of(() -> usersConnectionRepository.createConnectionRepository(user.getId()))
                .flatMap(connectionRepository -> Try.of(() -> connectionRepository.getPrimaryConnection(Twitter.class)));
    }

    @Override
    public Try<Integer> deleteAllByUser(final User user) {
        requireNonNull(user, "user is null");

        return Try.of(() -> customUserConnectionRepository.deleteAllByUserId(user.getId()));
    }
}
