package com.datascouting.dachs.alert.service.chart.charts.conversation.health;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

import java.util.Date;

public interface ConversationHealthChartService {

    Try<ConversationHealthResponse> getConversationHealth(User user, Date startDate, Date endDate);
}
