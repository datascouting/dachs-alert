package com.datascouting.dachs.alert.service.alert.report.schedule.context;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.Tuple2;
import javaslang.control.Try;

import java.util.concurrent.ScheduledFuture;

public interface CronServiceContext {

    Try<Void> addScheduledFuture(Tuple2<User, ScheduledFuture> userScheduledFuture);

    Try<Void> removeScheduledFuture(User user);

    Try<Void> removeScheduledFuture(String userId);
}
