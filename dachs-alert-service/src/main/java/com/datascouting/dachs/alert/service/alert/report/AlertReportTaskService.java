package com.datascouting.dachs.alert.service.alert.report;

import com.datascouting.dachs.alert.domain.entity.User;

public interface AlertReportTaskService {

    Runnable generateTask(User user);
}
