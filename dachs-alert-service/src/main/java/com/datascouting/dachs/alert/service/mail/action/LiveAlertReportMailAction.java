package com.datascouting.dachs.alert.service.mail.action;

import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.collection.List;
import javaslang.control.Try;

import java.util.Locale;

public interface LiveAlertReportMailAction {

    Try<Void> send(User user, List<TweetHateDetection> tweetHateDetections, Locale locale);
}
