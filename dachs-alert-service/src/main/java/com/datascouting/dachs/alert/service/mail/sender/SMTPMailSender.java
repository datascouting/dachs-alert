package com.datascouting.dachs.alert.service.mail.sender;

import com.datascouting.dachs.alert.service.mail.model.Mail;
import com.datascouting.dachs.alert.service.mail.preparator.MailPreparator;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import static java.util.Objects.requireNonNull;

@Slf4j
@RequiredArgsConstructor
@Service
public class SMTPMailSender implements MailSender {

    private final JavaMailSender mailSender;

    @Override
    public Try<Void> send(Mail mail) {
        requireNonNull(mail, "mail is null");
        log.info("Sending mail...");

        return Try.of(() -> MailPreparator.prepare(mail))
                .flatMap(mimeMessagePreparator -> Try.run(() -> mailSender.send(mimeMessagePreparator)))
                .onFailure(throwable -> log.warn("Could not send email: " + mail.toString(), throwable))
                .onSuccess(aVoid -> log.info("Successfully send mail!"));
    }
}
