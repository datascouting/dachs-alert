package com.datascouting.dachs.alert.service.chart.server.rendering.trend;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.chart.charts.trend.TrendChartService;
import com.datascouting.dachs.alert.service.chart.charts.trend.TrendResponse;
import com.datascouting.dachs.alert.service.chart.server.rendering.html.screenshot.HtmlScreenshotService;
import com.datascouting.dachs.alert.service.chart.server.rendering.util.ChartDataScriptUtil;
import com.datascouting.dachs.alert.service.chart.server.rendering.util.FileUtils;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import javaslang.control.Try;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.nio.file.Path;
import java.util.Date;

import static java.util.Objects.requireNonNull;

@Service
@AllArgsConstructor
public class TrendChartServerRenderingServiceImpl implements TrendChartServerRenderingService {

    private final TrendChartService trendChartService;
    private final TemplateEngine templateEngine;
    private final HtmlScreenshotService htmlScreenshotService;
    private final DachsAlertConfigurationProperties properties;

    @Override
    public Try<Path> getImagePath(final User user,
                                  final Date startDate,
                                  final Date endDate,
                                  final Integer intervalMinutes) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");
        requireNonNull(intervalMinutes, "intervalMinutes is null");

        return trendChartService.getTrend(user, startDate, endDate, intervalMinutes)
                .flatMap(this::createContext)
                .map(context -> templateEngine.process(
                        "mail/chart/trend",
                        context
                ))
                .flatMap(html -> FileUtils.createTempHtmlFile()
                        .flatMap(tempFile -> FileUtils.writeToFile(tempFile, html))
                        .flatMap(htmlFile -> FileUtils.createEmailImagePath(properties.getStoragePath())
                                .flatMap(emailImagePath -> htmlScreenshotService.screenshot(
                                        htmlFile.toPath(),
                                        emailImagePath,
                                        800,
                                        420
                                ))))
                .flatMap(file -> Try.of(file::toPath));
    }

    private Try<Context> createContext(final TrendResponse trendResponse) {
        requireNonNull(trendResponse, "trendResponse is null");

        return ChartDataScriptUtil.createTrendData(trendResponse)
                .map(script -> {
                    final Context context = new Context();
                    context.setVariable("trendDataScript", script);
                    return context;
                });
    }
}
