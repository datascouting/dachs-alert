package com.datascouting.dachs.alert.service.microservice.detection.api;

import com.datascouting.dachs.alert.service.microservice.detection.api.model.DetectionResponse;
import javaslang.control.Try;
import org.springframework.social.twitter.api.Tweet;

public interface DetectionApiClient {

    Try<DetectionResponse> detect(Tweet tweet);
}
