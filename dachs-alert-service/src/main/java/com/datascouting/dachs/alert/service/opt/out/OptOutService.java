package com.datascouting.dachs.alert.service.opt.out;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

public interface OptOutService {

    Try<Void> optOut(User user);
}
