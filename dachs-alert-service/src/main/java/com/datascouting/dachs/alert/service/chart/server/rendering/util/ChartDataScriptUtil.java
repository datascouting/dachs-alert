package com.datascouting.dachs.alert.service.chart.server.rendering.util;

import com.datascouting.dachs.alert.service.chart.charts.conversation.health.ConversationHealthResponse;
import com.datascouting.dachs.alert.service.chart.charts.trend.TrendResponse;
import javaslang.collection.List;
import javaslang.control.Try;

import static java.util.Objects.requireNonNull;

public class ChartDataScriptUtil {

    public static Try<String> createTrendData(final TrendResponse trendResponse) {
        requireNonNull(trendResponse, "trendResponse is null");

        return Try.of(() -> {
            final String string = List.ofAll(trendResponse.getData())
                    .foldLeft("", (s, trendRow) -> s + "{" +
                            "'id': " + trendRow.getId() + ", " +
                            "'hateSpeech': " + trendRow.getHateSpeech() + ", " +
                            "'hateSpeechSum': " + trendRow.getHateSpeechSum() + ", " +
                            "'personalAttack': " + trendRow.getPersonalAttack() + ", " +
                            "'personalAttackSum': " + trendRow.getPersonalAttackSum() + ", " +
                            "},")
                    .trim();

            return new StringBuilder(string)
                    .insert(0, "[")
                    .append("];")
                    .insert(0, "var data = ")
                    .toString();
        });
    }

    public static Try<String> createConversationHealthData(final ConversationHealthResponse conversationHealthResponse) {
        requireNonNull(conversationHealthResponse, "conversationHealthResponse is null");

        return Try.of(() -> {
            final String string = List.ofAll(conversationHealthResponse.getData())
                    .foldLeft("", (s, metricRow) -> s + "{" +
                            "'id': " + metricRow.getId() + ", " +
                            "'hateSpeech': " + metricRow.getHateSpeech() + ", " +
                            "'personalAttack': " + metricRow.getPersonalAttack() + ", " +
                            "},")
                    .trim();

            return new StringBuilder(string)
                    .insert(0, "[")
                    .append("];")
                    .insert(0, "var data = ")
                    .toString();
        });
    }
}
