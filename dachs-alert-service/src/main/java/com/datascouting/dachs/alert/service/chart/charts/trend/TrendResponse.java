package com.datascouting.dachs.alert.service.chart.charts.trend;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class TrendResponse {

    private List<TrendRow> data;

    @Data
    @Builder
    public static class TrendRow {

        private Long id;

        private Double hateSpeech;
        private Double hateSpeechSum;

        private Double personalAttack;
        private Double personalAttackSum;
    }
}
