package com.datascouting.dachs.alert.service.alert.report.schedule;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.collection.List;
import javaslang.control.Try;

public interface CronScheduler {

    Try<Void> schedule(User user);

    Try<Void> schedule(List<User> users);

    Try<Void> unschedule(User user);

    Try<Void> unschedule(String userId);
}
