package com.datascouting.dachs.alert.service.chart.server.rendering.trend;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

import java.nio.file.Path;
import java.util.Date;

public interface TrendChartServerRenderingService {

    Try<Path> getImagePath(User user, Date startDate, Date endDate, Integer intervalMinutes);
}
