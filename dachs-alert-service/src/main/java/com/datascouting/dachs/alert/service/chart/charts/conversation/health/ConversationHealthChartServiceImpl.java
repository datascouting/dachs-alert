package com.datascouting.dachs.alert.service.chart.charts.conversation.health;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import com.datascouting.dachs.alert.domain.entity.QTweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.crud.system.TweetHateDetectionService;
import com.querydsl.core.types.Predicate;
import javaslang.Value;
import javaslang.collection.List;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

import static java.util.Objects.requireNonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class ConversationHealthChartServiceImpl implements ConversationHealthChartService {

    private final TweetHateDetectionService tweetHateDetectionService;

    @Override
    public Try<ConversationHealthResponse> getConversationHealth(final User user,
                                                                 final Date startDate,
                                                                 final Date endDate) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");

        return createPredicate(user, startDate, endDate)
                .flatMap(predicate -> tweetHateDetectionService.findAll(
                        predicate,
                        new Sort(Sort.Direction.ASC, "createdAt"
                        )))
                .flatMap(this::createMetricsData)
                .flatMap(this::groupMetricsData)
                .onFailure(throwable -> log.error("could not create conversation health data", throwable));
    }

    private Try<Predicate> createPredicate(final User user,
                                           final Date startDate,
                                           final Date endDate) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");

        return Try.of(() -> {
            final QTweetHateDetection tweetHateDetection = QTweetHateDetection.tweetHateDetection;

            return tweetHateDetection.createdAt.between(startDate, endDate)
                    .and(tweetHateDetection.hateDetectionStatus.eq(HateDetectionStatus.COMPLETED))
                    .and(tweetHateDetection.user.id.eq(user.getId()));
        });
    }

    private Try<ConversationHealthResponse> createMetricsData(final List<TweetHateDetection> tweetHateDetections) {
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");

        return Try.sequence(tweetHateDetections.map(this::createMetricsDataRow))
                .map(Value::toList)
                .map(metricRows -> metricRows.map(this::processMetricRow))
                .map(metricRows -> ConversationHealthResponse.builder()
                        .data(metricRows)
                        .build());
    }

    private ConversationHealthResponse.MetricRow processMetricRow(final ConversationHealthResponse.MetricRow metricRow) {
        requireNonNull(metricRow, "metricRow is null");

        Try.of(() -> String.format("%1$,.2f", metricRow.getHateSpeech() * 100))
                .flatMap(string -> Try.of(() -> Double.valueOf(string)))
                .forEach(metricRow::setHateSpeech);

        Try.of(() -> String.format("%1$,.2f", metricRow.getPersonalAttack() * 100))
                .flatMap(string -> Try.of(() -> Double.valueOf(string)))
                .forEach(metricRow::setPersonalAttack);

        return metricRow;
    }

    private Try<ConversationHealthResponse.MetricRow> createMetricsDataRow(final TweetHateDetection tweetHateDetection) {
        requireNonNull(tweetHateDetection, "tweetHateDetection is null");

        return Try.of(() -> ConversationHealthResponse.MetricRow.builder()
                .id(tweetHateDetection.getCreatedAt().getTime())
                .hateSpeech(tweetHateDetection.getHateSpeechRate())
                .personalAttack(tweetHateDetection.getPersonalAttackRate())
                .build());
    }

    private Try<ConversationHealthResponse> groupMetricsData(final ConversationHealthResponse conversationHealthResponse) {
        requireNonNull(conversationHealthResponse, "conversationHealthResponse is null");

        Double groupLimit = 20.0;

        return Try.of(conversationHealthResponse::getData)
                .flatMap(metricRows -> Try.of(() -> metricRows
                        .foldLeft(new ArrayList<java.util.List<ConversationHealthResponse.MetricRow>>(), (state, metricRow) -> {
                            if (state.size() == 0) {
                                final ArrayList<ConversationHealthResponse.MetricRow> rowsForState = new ArrayList<>();
                                rowsForState.add(metricRow);
                                state.add(rowsForState);
                            } else {
                                final int lastStateIndex = state.size() - 1;
                                final java.util.List<ConversationHealthResponse.MetricRow> rows = state.get(lastStateIndex);

                                final int lastRowsIndex = rows.size() - 1;

                                final ConversationHealthResponse.MetricRow lastRow = rows.get(lastRowsIndex);

                                if (lastRow.getPersonalAttack() > groupLimit
                                        || lastRow.getHateSpeech() > groupLimit) {
                                    final ArrayList<ConversationHealthResponse.MetricRow> rowsForState = new ArrayList<>();
                                    rowsForState.add(metricRow);
                                    state.add(rowsForState);
                                } else {
                                    rows.add(metricRow);
//                                    state.add(lastStateIndex, rows);
                                }
                            }

                            return state;
                        })))
                .map(List::ofAll)
                .map(metricRows -> metricRows
                        .foldLeft(new ArrayList<ConversationHealthResponse.MetricRow>(), (state, list) -> {
                            if (list.size() > 1) {
                                final ConversationHealthResponse.MetricRow first = list.get(0);
                                final ConversationHealthResponse.MetricRow last = list.get(list.size() - 1);
                                state.add(first);
                                state.add(last);
                            } else {
                                state.addAll(list);
                            }

                            return state;
                        }))
                .map(List::ofAll)
                .map(metricRows -> {
                    conversationHealthResponse.setData(metricRows);
                    return conversationHealthResponse;
                });
    }
}
