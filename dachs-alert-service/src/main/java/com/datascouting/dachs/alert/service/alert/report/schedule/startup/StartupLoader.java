package com.datascouting.dachs.alert.service.alert.report.schedule.startup;

import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.datascouting.dachs.alert.service.alert.report.schedule.CronScheduler;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class StartupLoader {

    private final CronScheduler cronScheduler;
    private final UserService userService;

    @EventListener(ContextRefreshedEvent.class)
    public void contextRefreshedEvent() {
        scheduleCronTasks()
                .onFailure(throwable -> log.error("Could not schedule alerts", throwable));
    }

    private Try<Void> scheduleCronTasks() {
        return userService.findAllByActiveReports(true)
                .flatMap(cronScheduler::schedule);
    }
}
