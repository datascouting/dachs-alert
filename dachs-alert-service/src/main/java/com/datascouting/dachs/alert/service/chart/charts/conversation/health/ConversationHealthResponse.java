package com.datascouting.dachs.alert.service.chart.charts.conversation.health;

import javaslang.collection.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ConversationHealthResponse {

    private List<MetricRow> data;

    @Data
    @Builder
    public static class MetricRow {

        private Long id;
        private Double hateSpeech;
        private Double personalAttack;
    }
}
