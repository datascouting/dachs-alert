package com.datascouting.dachs.alert.service;

import com.datascouting.dachs.detection.sdk.DetectionSdkConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@ComponentScan
@Import(DetectionSdkConfiguration.class)
public class DachsAlertServiceConfiguration {

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        final ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(100);
        threadPoolTaskScheduler.setThreadNamePrefix("DachsAlertThreadPoolTaskScheduler");
        return threadPoolTaskScheduler;
    }
}
