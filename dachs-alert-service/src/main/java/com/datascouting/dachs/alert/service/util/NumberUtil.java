package com.datascouting.dachs.alert.service.util;

import javaslang.collection.List;

import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.*;

public class NumberUtil {

    public static double sum(final List<Double> doubles) {
        requireNonNull(doubles, "doubles is null");

        return doubles.map(BigDecimal::valueOf)
                .foldLeft(BigDecimal.ZERO, BigDecimal::add)
                .doubleValue();
    }
}
