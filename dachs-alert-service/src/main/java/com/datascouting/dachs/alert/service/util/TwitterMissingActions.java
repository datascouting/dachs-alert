package com.datascouting.dachs.alert.service.util;

import javaslang.collection.List;
import javaslang.control.Option;
import javaslang.control.Try;
import org.springframework.social.connect.Connection;
import org.springframework.social.support.URIBuilder;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;

import java.net.URI;
import java.util.ArrayList;

import static java.util.Objects.requireNonNull;

public class TwitterMissingActions {

    private static final String API_URL_BASE = "https://api.twitter.com/1.1/";

    public static Try<List<Tweet>> statusesLookup(final Connection<Twitter> twitterConnection,
                                                  final List<Long> tweetIds) {
        requireNonNull(twitterConnection, "twitterConnection is null");
        requireNonNull(tweetIds, "tweetIds is null");

        return Try.of(() -> {
            final RestOperations restOperations = twitterConnection.getApi()
                    .restOperations();

            final MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
            parameters.set("id", String.join(",", tweetIds.map(String::valueOf)));
            parameters.set("include_entities", "true");

            return Option.of(restOperations.getForObject(buildUri("statuses/lookup.json", parameters), TweetList.class))
                    .map(List::ofAll)
                    .getOrElse(List::empty);

        });
    }

    private static URI buildUri(final String path,
                                final MultiValueMap<String, String> parameters) {
        requireNonNull(path, "path is null");
        requireNonNull(parameters, "parameters is null");

        return URIBuilder
                .fromUri(API_URL_BASE + path)
                .queryParams(parameters)
                .build();
    }

    private static class TweetList extends ArrayList<Tweet> {
    }
}
