package com.datascouting.dachs.alert.service.chart.server.rendering.conversation.health;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.chart.charts.conversation.health.ConversationHealthChartService;
import com.datascouting.dachs.alert.service.chart.charts.conversation.health.ConversationHealthResponse;
import com.datascouting.dachs.alert.service.chart.server.rendering.html.screenshot.HtmlScreenshotService;
import com.datascouting.dachs.alert.service.chart.server.rendering.util.ChartDataScriptUtil;
import com.datascouting.dachs.alert.service.chart.server.rendering.util.FileUtils;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import javaslang.control.Try;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.nio.file.Path;
import java.util.Date;

import static java.util.Objects.requireNonNull;

@Service
@AllArgsConstructor
public class ConversationHealthChartServerRenderingServiceImpl implements ConversationHealthChartServerRenderingService {

    private final ConversationHealthChartService conversationHealthChartService;
    private final TemplateEngine templateEngine;
    private final HtmlScreenshotService htmlScreenshotService;
    private final DachsAlertConfigurationProperties properties;

    @Override
    public Try<Path> getImagePath(final User user, final Date startDate, final Date endDate) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");

        return conversationHealthChartService.getConversationHealth(user, startDate, endDate)
                .flatMap(this::createContext)
                .map(context -> templateEngine.process(
                        "mail/chart/conversation-health",
                        context
                ))
                .flatMap(html -> FileUtils.createTempHtmlFile()
                        .flatMap(tempFile -> FileUtils.writeToFile(tempFile, html))
                        .flatMap(htmlFile -> FileUtils.createEmailImagePath(properties.getStoragePath())
                                .flatMap(emailImagePath -> htmlScreenshotService.screenshot(
                                        htmlFile.toPath(),
                                        emailImagePath,
                                        800,
                                        420
                                ))))
                .flatMap(file -> Try.of(file::toPath));
    }

     private Try<Context> createContext(final ConversationHealthResponse conversationHealthResponse) {
        requireNonNull(conversationHealthResponse, "conversationHealthResponse is null");

        return ChartDataScriptUtil.createConversationHealthData(conversationHealthResponse)
                .map(script -> {
                    final Context context = new Context();
                    context.setVariable("conversationHealthDataScript", script);
                    return context;
                });
    }
}
