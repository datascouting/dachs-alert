package com.datascouting.dachs.alert.service.monitoring;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.crud.custom.UserConnectionService;
import com.datascouting.dachs.alert.service.crud.system.TweetHateDetectionService;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.datascouting.dachs.alert.service.microservice.detection.api.DetectionApiClient;
import com.datascouting.dachs.alert.service.microservice.detection.api.model.DetectionResponse;
import javaslang.collection.List;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class MonitorTwitterTimelinesImpl implements MonitorTwitterTimelines {

    private final UserService userService;
    private final TweetHateDetectionService tweetHateDetectionService;
    private final DetectionApiClient detectionApiClient;
    private final UserConnectionService userConnectionService;

    @Override
    @Scheduled(cron = "${dachs.monitor-twitter-timelines-cron}")
    @Transactional
    public void persistTimelines() {
        userService.findAllByActiveMonitoring(true)
                .flatMap(users -> Try.sequence(users.map(this::persistUserTimeline)))
                .map(lists -> null);
    }

    private Try<List<TweetHateDetection>> persistUserTimeline(final User user) {
        requireNonNull(user, "user is null");

        return fetchUserTimeLine(user)
                .onFailure(throwable -> log.error(String.format("Could not fetch home timeline for user %s", user.getId()), throwable))
                .flatMap(tweets -> persistTwitterHateDetections(tweets, user)
                        .onFailure(throwable -> log.error(String.format("Could not persist tweets for user %s", user.getId()), throwable))
                        .flatMap(tweetHateDetections -> persistUserLastHomeTimelineTweetId(tweetHateDetections, user)
                                .onFailure(throwable -> log.error(String.format("Could not persist user last home timeline tweet id for user %s", user.getId()), throwable))));
    }

    private Try<List<Tweet>> fetchUserTimeLine(final User user) {
        requireNonNull(user, "user is null");

        return userConnectionService.getTwitterConnection(user)
                .onFailure(throwable -> log.error(String.format("Could not get twitter connection for user %s", user.getId()), throwable))
                .flatMap(twitterConnection -> Try.of(() -> twitterConnection.getApi()
                        .timelineOperations()
                        .getHomeTimeline(200, user.getLastHomeTimelineTweetId(), 0)))
                .map(List::ofAll);
    }

    private Try<List<TweetHateDetection>> persistTwitterHateDetections(final List<Tweet> tweets,
                                                                       final User user) {
        requireNonNull(tweets, "tweets is null");
        requireNonNull(user, "user is null");

        return Try.of(() -> tweets
                .flatMap(tweet -> createTweetHateDetection(tweet, user)))
                .flatMap(tweetHateDetectionService::save);
    }

    private Try<TweetHateDetection> createTweetHateDetection(final Tweet tweet,
                                                             final User user) {
        requireNonNull(tweet, "tweet is null");
        requireNonNull(user, "user is null");

        return detectionApiClient.detect(tweet)
                .onFailure(throwable -> log.error(String.format("Could not execute detection via Detection Api for tweet id %d, error message %s", tweet.getId(), throwable.getMessage())))
                .flatMap(detectionResponse -> createCompletedTweetHateDetection(detectionResponse, user))
                .recoverWith(throwable -> createPendingTweetHateDetection(tweet, user));
    }

    private Try<TweetHateDetection> createCompletedTweetHateDetection(final DetectionResponse detectionResponse,
                                                                      final User user) {
        requireNonNull(detectionResponse, "detectionResponse is null");
        requireNonNull(user, "user is null");

        return Try.of(() -> TweetHateDetection.builder()
                .user(user)
                .hateDetectionStatus(HateDetectionStatus.COMPLETED)
                .retries(0)
                .tweetId(detectionResponse.getTweetId())
                .hateSpeechRate(detectionResponse.getHateSpeechRate())
                .personalAttackRate(detectionResponse.getPersonalAttackRate())
                .build());
    }

    private Try<TweetHateDetection> createPendingTweetHateDetection(final Tweet tweet,
                                                                    final User user) {
        requireNonNull(tweet, "tweet is null");
        requireNonNull(user, "user is null");

        return Try.of(() -> TweetHateDetection.builder()
                .user(user)
                .tweetId(tweet.getId())
                .hateDetectionStatus(HateDetectionStatus.PENDING)
                .retries(0)
                .build());
    }

    private Try<List<TweetHateDetection>> persistUserLastHomeTimelineTweetId(final List<TweetHateDetection> tweetHateDetections,
                                                                             final User user) {
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");
        requireNonNull(user, "user is null");

        return findUserLastHomeTimelineTweetId(tweetHateDetections, user.getLastHomeTimelineTweetId())
                .onFailure(throwable -> log.error(String.format("Could not find last home timeline tweet id for user %s", user.getId()), throwable))
                .flatMap(userLastHomeTimelineTweetId -> {
                    user.setLastHomeTimelineTweetId(userLastHomeTimelineTweetId);

                    return userService.update(user)
                            .onFailure(throwable -> log.error(String.format("Could not update user %s", user.getId()), throwable));
                })
                .map(ignored -> tweetHateDetections);
    }

    private Try<Long> findUserLastHomeTimelineTweetId(final List<TweetHateDetection> tweetHateDetections,
                                                      final Long currentTwitterId) {
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");
        requireNonNull(currentTwitterId, "currentTwitterId is null");

        return tweetHateDetections
                .map(TweetHateDetection::getTweetId)
                .prepend(currentTwitterId)
                .max()
                .toTry();
    }
}
