package com.datascouting.dachs.alert.service.chart.server.rendering.conversation.health;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

import java.nio.file.Path;
import java.util.Date;

public interface ConversationHealthChartServerRenderingService {

    Try<Path> getImagePath(User user, Date startDate, Date endDate);
}
