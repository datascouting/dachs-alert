package com.datascouting.dachs.alert.service.chart.charts.trend;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

import java.util.Date;

public interface TrendChartService {

    Try<TrendResponse> getTrend(User user, Date startDate, Date endDate, Integer intervalMinutes);
}
