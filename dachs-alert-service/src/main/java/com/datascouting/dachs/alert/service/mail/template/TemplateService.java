package com.datascouting.dachs.alert.service.mail.template;

import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.collection.List;
import javaslang.control.Try;

import java.io.File;
import java.util.Date;
import java.util.Locale;

public interface TemplateService {

    Try<String> buildLiveAlertReportTemplate(User user, List<TweetHateDetection> tweetHateDetections, Locale locale);

    Try<String> buildAlertReportTemplate(User user, String from, String to, Locale locale);
}
