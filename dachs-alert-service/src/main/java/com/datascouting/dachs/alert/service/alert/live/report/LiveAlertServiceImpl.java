package com.datascouting.dachs.alert.service.alert.live.report;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import com.datascouting.dachs.alert.domain.entity.QTweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import com.datascouting.dachs.alert.service.crud.system.TweetHateDetectionService;
import com.datascouting.dachs.alert.service.crud.system.UserService;
import com.datascouting.dachs.alert.service.mail.action.LiveAlertReportMailAction;
import com.datascouting.dachs.alert.service.util.ThresholdUtil;
import com.querydsl.core.types.dsl.BooleanExpression;
import javaslang.Tuple;
import javaslang.Tuple2;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Locale;

import static com.datascouting.dachs.alert.service.util.ThresholdUtil.MEDIUM_THRESHOLD;

@Slf4j
@Service
@RequiredArgsConstructor
public class LiveAlertServiceImpl implements LiveAlertService {

    private final UserService userService;
    private final TweetHateDetectionService tweetHateDetectionService;
    private final DachsAlertConfigurationProperties properties;

    private final LiveAlertReportMailAction liveAlertReportMailAction;

    @Override
    @Scheduled(fixedDelayString = "${dachs.live-alert-interval}")
    @Transactional
    public void process() {
        userService.findAllByActiveLiveAlerts(true)
                .forEach(users -> users.forEach(user -> {
                    final BooleanExpression predicate = getLastTweetsPredicate(user);

                    tweetHateDetectionService.findAll(predicate)
                            .forEach(tweetHateDetections -> {
                                if (!tweetHateDetections.isEmpty()) {
                                    liveAlertReportMailAction.send(
                                            user,
                                            tweetHateDetections,
                                            Locale.ENGLISH
                                    );
                                }
                            });
                }));
    }

    private BooleanExpression getLastTweetsPredicate(final User user) {
        final Tuple2<Date, Date> fromToDates = getFromToDates();

        final Double threshold = ThresholdUtil.translateToRate(user.getHateSpeechThreshold())
                .getOrElse(() -> MEDIUM_THRESHOLD);

        final QTweetHateDetection tweetHateDetection = QTweetHateDetection.tweetHateDetection;

        return tweetHateDetection.hateDetectionStatus.eq(HateDetectionStatus.COMPLETED)
                .and(tweetHateDetection.user.id.eq(user.getId()))
                .and(tweetHateDetection.createdAt.between(fromToDates._1, fromToDates._2))
                .and(tweetHateDetection.personalAttackRate.goe(threshold))
                .and(tweetHateDetection.hateSpeechRate.goe(threshold));
    }

    private Tuple2<Date, Date> getFromToDates() {
        final Date to = new Date();
        final Date from = new Date(to.getTime() - properties.getLiveAlertInterval());

        return Tuple.of(from, to);
    }
}
