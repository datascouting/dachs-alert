package com.datascouting.dachs.alert.service.chart.server.rendering.html.screenshot;

import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import javaslang.Tuple;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Arrays;

import static java.util.Objects.requireNonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class HtmlScreenshotServiceImpl implements HtmlScreenshotService {

    private final DachsAlertConfigurationProperties properties;

    @Override
    public Try<File> screenshot(final Path htmlPath,
                                final Path destinationPath,
                                final Integer viewportWidth,
                                final Integer viewportHeight) {
        requireNonNull(htmlPath, "htmlPath is null");
        requireNonNull(destinationPath, "destinationPath is null");
        requireNonNull(viewportWidth, "viewportWidth is null");
        requireNonNull(viewportHeight, "viewportHeight is null");

        return createCommandString(htmlPath, destinationPath, viewportWidth, viewportHeight)
                .flatMap(command -> Try.of(() -> {
                    final Runtime runtime = Runtime.getRuntime();
                    return runtime.exec(command);
                }))
                .flatMap(process -> Try.of(() -> {
                    final BufferedReader stdInput = new BufferedReader(new
                            InputStreamReader(process.getInputStream()));

                    final BufferedReader stdError = new BufferedReader(new
                            InputStreamReader(process.getErrorStream()));

                    final StringBuilder resultStringBuilder = new StringBuilder();
                    String resultLine;
                    while ((resultLine = stdInput.readLine()) != null) {
                        resultStringBuilder.append(resultLine);
                    }

                    final StringBuilder errorStringBuilder = new StringBuilder();
                    String errorLine;
                    System.out.println("Here is the standard error of the command (if any):\n");
                    while ((errorLine = stdError.readLine()) != null) {
                        errorStringBuilder.append(errorLine);
                    }

                    return Tuple.of(resultStringBuilder.toString(), errorStringBuilder.toString());
                }))
                .flatMap(resultError -> {
                    final String result = resultError._1;
                    final String error = resultError._2;

                    log.info("html-screenshot:" + result);

                    if (!error.trim().equals("")) {
                        log.error("Errors:" + error);
                        return Try.failure(new RuntimeException("An error occurred: " + error));
                    } else {
                        return Try.success(destinationPath.toFile());
                    }
                });
    }

    private Try<String> createCommandString(final Path htmlPath,
                                            final Path destinationPath,
                                            final Integer viewportWidth,
                                            final Integer viewportHeight) {
        requireNonNull(htmlPath, "htmlPath is null");
        requireNonNull(destinationPath, "destinationPath is null");
        requireNonNull(viewportWidth, "viewportWidth is null");
        requireNonNull(viewportHeight, "viewportHeight is null");

        return Option.of(properties.getHtmlScreenshotCommand())
                .toTry()
                .flatMap(command -> Try.of(() -> String.join(" ", Arrays.asList(
                        command,
                        "-d", destinationPath.toString(),
                        "--htmlPath", htmlPath.toUri().toString(),
                        "--viewportWidth", viewportWidth.toString(),
                        "--viewportHeight", viewportHeight.toString()
                ))))
                .peek(log::info);
    }
}
