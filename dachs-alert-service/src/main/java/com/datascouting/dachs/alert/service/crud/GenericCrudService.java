package com.datascouting.dachs.alert.service.crud;

import org.intellift.sol.domain.Identifiable;
import org.intellift.sol.service.querydsl.QueryDslCrudService;

public interface GenericCrudService<E extends Identifiable<String>> extends QueryDslCrudService<E, String> {
}
