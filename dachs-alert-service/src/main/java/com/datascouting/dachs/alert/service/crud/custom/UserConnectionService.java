package com.datascouting.dachs.alert.service.crud.custom;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;
import org.springframework.social.connect.Connection;
import org.springframework.social.twitter.api.Twitter;

public interface UserConnectionService {

    Try<Connection<Twitter>> getTwitterConnection(User user);

    Try<Integer> deleteAllByUser(User user);
}
