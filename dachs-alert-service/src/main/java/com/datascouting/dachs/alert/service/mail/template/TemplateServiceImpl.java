package com.datascouting.dachs.alert.service.mail.template;

import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.alert.live.report.model.LiveDetection;
import com.datascouting.dachs.alert.service.util.ThresholdUtil;
import javaslang.Value;
import javaslang.collection.List;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.HashMap;
import java.util.Locale;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class TemplateServiceImpl implements TemplateService {

    private final TemplateEngine templateEngine;
    private final MessageSource messageSource;

    @Override
    public Try<String> buildLiveAlertReportTemplate(final User user,
                                                    final List<TweetHateDetection> tweetHateDetections,
                                                    final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");
        requireNonNull(locale, "locale is null");

        return createLiveAlertReportTemplateMailContext(user, tweetHateDetections, locale)
                .map(context -> templateEngine.process(
                        "mail/live-alert-report",
                        context
                ));
    }

    @Override
    public Try<String> buildAlertReportTemplate(final User user,
                                                final String from,
                                                final String to,
                                                final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(from, "from is null");
        requireNonNull(to, "to is null");
        requireNonNull(locale, "locale is null");

        return createAlertReportTemplateMailContext(user, from, to, locale)
                .map(context -> templateEngine.process(
                        "mail/alert-report",
                        context
                ));
    }

    private Try<Context> createLiveAlertReportTemplateMailContext(final User user,
                                                                  final List<TweetHateDetection> tweetHateDetections,
                                                                  final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");
        requireNonNull(locale, "locale is null");

        return mapToLiveDetection(tweetHateDetections, locale)
                .flatMap(liveDetections -> Try.of(() -> {
                    final HashMap<String, Object> variables = new HashMap<>();
                    variables.put("user", user);
                    variables.put("liveDetections", liveDetections);

                    return variables;
                }))
                .flatMap(variables -> Try.of(() -> new Context(locale, variables)));
    }

    private Try<Context> createAlertReportTemplateMailContext(final User user,
                                                              final String from,
                                                              final String to,
                                                              final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(from, "from is null");
        requireNonNull(to, "to is null");
        requireNonNull(locale, "locale is null");

        return Try.of(() -> {
            final HashMap<String, Object> variables = new HashMap<>();
            variables.put("user", user);
            variables.put("from", from);
            variables.put("to", to);

            return variables;
        })
                .flatMap(variables -> Try.of(() -> new Context(locale, variables)));
    }

    private Try<List<LiveDetection>> mapToLiveDetection(final List<TweetHateDetection> tweetHateDetections,
                                                        final Locale locale) {
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");
        requireNonNull(locale, "locale is null");

        return Try.sequence(tweetHateDetections.map(tweetHateDetection -> mapToLiveDetection(tweetHateDetection, locale)))
                .map(Value::toList);
    }

    private Try<LiveDetection> mapToLiveDetection(final TweetHateDetection tweetHateDetection,
                                                  final Locale locale) {
        requireNonNull(tweetHateDetection, "tweetHateDetection is null");
        requireNonNull(locale, "locale is null");

        return ThresholdUtil.translateToText(tweetHateDetection.getHateSpeechRate())
                .flatMap(hateSpeech -> Try.of(() -> messageSource.getMessage(hateSpeech, null, locale)))
                .flatMap(hateSpeech -> ThresholdUtil.translateToText(tweetHateDetection.getPersonalAttackRate())
                        .flatMap(personalAttack -> Try.of(() -> messageSource.getMessage(personalAttack, null, locale)))
                        .flatMap(personalAttack -> Try.of(() -> LiveDetection.builder()
                                .tweetId(tweetHateDetection.getTweetId())
                                .createdAt(tweetHateDetection.getCreatedAt())
                                .hateSpeech(hateSpeech)
                                .personalAttack(personalAttack)
                                .build())));
    }
}
