package com.datascouting.dachs.alert.service.mail.action;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

import java.io.File;
import java.util.Date;
import java.util.Locale;

public interface AlertReportMailAction {

    Try<Void> send(User user, Date from, Date to, File conversationHealthImageFile, File trendImageFile, Locale locale);
}
