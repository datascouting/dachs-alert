package com.datascouting.dachs.alert.service.alert.report.schedule.context;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.Tuple2;
import javaslang.control.Option;
import javaslang.control.Try;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import static java.util.Objects.requireNonNull;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CronServiceContextImpl implements CronServiceContext {

    private final Map<String, Tuple2<User, ScheduledFuture>> scheduledFutures = new HashMap<>();

    @Override
    public Try<Void> addScheduledFuture(final Tuple2<User, ScheduledFuture> scheduledFuture) {
        requireNonNull(scheduledFuture, "scheduledFuture is null");

        return removeScheduledFuture(scheduledFuture._1)
                .flatMap(aVoid -> Try.run(() -> scheduledFutures.put(scheduledFuture._1.getId(), scheduledFuture)));
    }

    @Override
    public Try<Void> removeScheduledFuture(final User user) {
        requireNonNull(user, "user is null");

        return removeScheduledFuture(user.getId());
    }

    @Override
    public Try<Void> removeScheduledFuture(final String userId) {
        requireNonNull(userId, "userId is null");

        return Try.run(() -> Option.of(scheduledFutures.get(userId))
                .map(alertDtoScheduledFuture -> alertDtoScheduledFuture._2)
                .forEach(scheduledFuture -> {
                    scheduledFuture.cancel(false);
                    scheduledFutures.remove(userId);
                }));
    }
}
