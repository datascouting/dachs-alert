package com.datascouting.dachs.alert.service.chart.server.rendering;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.chart.server.rendering.conversation.health.ConversationHealthChartServerRenderingService;
import com.datascouting.dachs.alert.service.chart.server.rendering.trend.TrendChartServerRenderingService;
import javaslang.control.Try;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.util.Date;

import static java.util.Objects.requireNonNull;

@Service
@AllArgsConstructor
public class ChartServerRenderingServiceImpl implements ChartServerRenderingService {

    private final TrendChartServerRenderingService trendChartServerRenderingService;
    private final ConversationHealthChartServerRenderingService conversationHealthChartServerRenderingService;

    @Override
    public Try<Path> createTrendChartImage(final User user,
                                           final Date startDate,
                                           final Date endDate,
                                           final Integer intervalMinutes) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");
        requireNonNull(intervalMinutes, "intervalMinutes is null");

        return trendChartServerRenderingService.getImagePath(user, startDate, endDate, intervalMinutes);
    }

    @Override
    public Try<Path> createConversationHealthChartImage(final User user,
                                                        final Date startDate,
                                                        final Date endDate) {
        requireNonNull(user, "user is null");
        requireNonNull(startDate, "startDate is null");
        requireNonNull(endDate, "endDate is null");

        return conversationHealthChartServerRenderingService.getImagePath(user, startDate, endDate);
    }
}
