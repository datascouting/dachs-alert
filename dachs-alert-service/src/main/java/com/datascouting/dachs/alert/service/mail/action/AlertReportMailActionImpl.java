package com.datascouting.dachs.alert.service.mail.action;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.mail.model.Mail;
import com.datascouting.dachs.alert.service.mail.sender.MailSender;
import com.datascouting.dachs.alert.service.mail.template.TemplateService;
import com.google.common.collect.Sets;
import javaslang.Tuple;
import javaslang.collection.List;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import static java.util.Objects.requireNonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class AlertReportMailActionImpl implements AlertReportMailAction {

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
    private final Environment environment;
    private final MailSender mailSender;
    private final TemplateService templateService;
    private final MessageSource messageSource;

    @Override
    public Try<Void> send(final User user,
                          final Date from,
                          final Date to,
                          final File conversationHealthImageFile,
                          final File trendImageFile,
                          final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(from, "from is null");
        requireNonNull(to, "to is null");
        requireNonNull(conversationHealthImageFile, "conversationHealthImageFile is null");
        requireNonNull(trendImageFile, "trendImageFile is null");
        requireNonNull(locale, "locale is null");

        final String fromDate = simpleDateFormat.format(from);
        final String toDate = simpleDateFormat.format(to);

        return templateService.buildAlertReportTemplate(user, fromDate, toDate, locale)
                .flatMap(html -> Try.of(() -> Mail.builder()
                        .to(Sets.newHashSet(user.getEmail()))
                        .from(environment.getProperty("spring.mail.username"))
                        .subject(String.format(
                                "%s: %s - %s", messageSource.getMessage("report-mail-subject", null, locale),
                                fromDate,
                                toDate
                        ))
                        .body(html)
                        .attachments(List.ofAll(Arrays.asList(
                                Tuple.of("conversationHealth", conversationHealthImageFile),
                                Tuple.of("trend", trendImageFile)
                        )))
                        .build())
                        .onFailure(throwable -> log.error("could not create mail", throwable))
                )
                .flatMap(mailSender::send)
                .onFailure(throwable -> log.error("could not create mail", throwable));
    }
}
