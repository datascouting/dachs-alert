package com.datascouting.dachs.alert.service.crud.system;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.alert.report.schedule.CronScheduler;
import com.datascouting.dachs.alert.service.crud.GenericCrudService;
import javaslang.collection.List;
import javaslang.control.Try;

public interface UserService extends GenericCrudService<User> {

    Try<List<User>> findAllByActiveReports(final Boolean activeReports);

    Try<List<User>> findAllByActiveMonitoring(final Boolean activeMonitoring);

    Try<List<User>> findAllByActiveLiveAlerts(final Boolean activeLiveAlerts);

    Try<Void> updateLastReportAt(String id);
}
