package com.datascouting.dachs.alert.service.mail.preparator;

import com.datascouting.dachs.alert.service.mail.model.Mail;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Objects;

@Slf4j
public class MailPreparator {

    public static MimeMessagePreparator prepare(final Mail mail) {
        Objects.requireNonNull(mail, "mail is null");

        return mimeMessage -> {
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage,
                    MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            message.setFrom(Option.of(mail.getFrom()).getOrElse(""));
            message.setText(Option.of(mail.getBody()).getOrElse(""), true);
            message.setSubject(Option.of(mail.getSubject()).getOrElse(""));
            message.setSentDate(new Date());

            Option.of(mail.getTo())
                    .map(to -> to.toArray(new String[0]))
                    .forEach(strings -> Try.run(() -> message.setTo(strings)));

            Option.of(mail.getBcc())
                    .map(bcc -> bcc.toArray(new String[0]))
                    .forEach(strings -> Try.run(() -> message.setBcc(strings)));

            Option.of(mail.getCc())
                    .map(cc -> cc.toArray(new String[0]))
                    .forEach(strings -> Try.run(() -> message.setCc(strings)));

            Option.of(mail.getAttachments())
                    .forEach(nameAttachmentFiles -> nameAttachmentFiles.forEach(nameAttachmentFile -> {
                        Try.run(() -> message.addInline(nameAttachmentFile._1, nameAttachmentFile._2))
                                .onFailure(throwable -> log.error("Could not add attachment", throwable));
                    }));
        };
    }
}
