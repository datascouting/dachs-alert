package com.datascouting.dachs.alert.service.crud.system;

import com.datascouting.dachs.alert.domain.entity.QUser;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.domain.repository.UserRepository;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import com.datascouting.dachs.alert.service.cron.CronEvaluation;
import com.querydsl.core.types.dsl.BooleanExpression;
import javaslang.Value;
import javaslang.collection.List;
import javaslang.collection.Traversable;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.intellift.sol.domain.querydsl.repository.QueryDslRepository;
import org.springframework.stereotype.Service;

import java.util.Date;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final DachsAlertConfigurationProperties dachsAlertConfigurationProperties;

    @Override
    public QueryDslRepository<User, String> getRepository() {
        return userRepository;
    }

    @Override
    public Try<User> save(final User entity) {
        requireNonNull(entity, "entity is null");

        final String cron = entity.getReportFrequencyCron();
        if (cron != null) {
            final String cronTrimmed = cron.trim();
            final List<Date> next = List.ofAll(CronEvaluation.getNext(cronTrimmed, 2));

            if (next.size() == 2) {
                final Date firstEvaluation = next.get(0);
                final Date secondEvaluation = next.get(1);

                final long durationInMinutes = (secondEvaluation.getTime() - firstEvaluation.getTime()) / 1000 / 60;
                if (durationInMinutes < dachsAlertConfigurationProperties.getMinCronDurationMinutes()) {
                    return Try.failure(new RuntimeException("Min cron duration surpassed"));
                }
            }
        }

        final Option<String> idOption = Option.of(entity.getId());

        final Try<Option<User>> userOptionTry;
        if (idOption.isDefined()) {
            userOptionTry = findOne(entity.getId());
        } else {
            userOptionTry = Try.success(Option.none());
        }

        return userOptionTry
                .map(userOption -> {
                    userOption.forEach(user -> {
                        entity.setPassword(user.getPassword());
                        entity.setLastHomeTimelineTweetId(user.getLastHomeTimelineTweetId());
                    });

                    if (entity.getLastHomeTimelineTweetId() == null) {
                        entity.setLastHomeTimelineTweetId(0L);
                    }

                    return entity;
                })
                .map(user -> userRepository.save(entity));
    }

    @Override
    public Try<List<User>> save(final Traversable<User> entities) {
        requireNonNull(entities, "entities is null");

        return Try.sequence(entities.map(this::save))
                .map(Value::toList);
    }

    @Override
    public Try<List<User>> findAllByActiveReports(final Boolean activeReports) {
        requireNonNull(activeReports, "activeReports is null");

        return Try.of(() -> {
            final BooleanExpression predicate = QUser.user.activeReports.eq(activeReports);

            return Option.of(userRepository.findAll(predicate))
                    .map(List::ofAll)
                    .getOrElse(List::empty);
        });
    }

    @Override
    public Try<List<User>> findAllByActiveMonitoring(Boolean activeMonitoring) {
        requireNonNull(activeMonitoring, "activeMonitoring is null");

        return Try.of(() -> {
            final BooleanExpression predicate = QUser.user.activeMonitoring.eq(activeMonitoring);

            return Option.of(userRepository.findAll(predicate))
                    .map(List::ofAll)
                    .getOrElse(List::empty);
        });
    }

    @Override
    public Try<List<User>> findAllByActiveLiveAlerts(final Boolean activeLiveAlerts) {
        requireNonNull(activeLiveAlerts, "activeLiveAlerts is null");

        return Try.of(() -> {
            final BooleanExpression predicate = QUser.user.activeLiveAlerts.eq(activeLiveAlerts);

            return Option.of(userRepository.findAll(predicate))
                    .map(List::ofAll)
                    .getOrElse(List::empty);
        });
    }

    @Override
    public Try<Void> updateLastReportAt(final String id) {
        requireNonNull(id, "id is null");

        return findOne(id)
                .flatMap(Value::toTry)
                .flatMap(user -> {
                    user.setLastReportAt(new Date());

                    return update(user);
                })
                .map(user -> null);
    }
}
