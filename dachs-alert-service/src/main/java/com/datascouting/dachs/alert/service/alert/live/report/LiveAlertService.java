package com.datascouting.dachs.alert.service.alert.live.report;

public interface LiveAlertService {

    void process();
}
