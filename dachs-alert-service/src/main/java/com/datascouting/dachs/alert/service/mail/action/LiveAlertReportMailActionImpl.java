package com.datascouting.dachs.alert.service.mail.action;

import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.mail.model.Mail;
import com.datascouting.dachs.alert.service.mail.sender.MailSender;
import com.datascouting.dachs.alert.service.mail.template.TemplateService;
import com.google.common.collect.Sets;
import javaslang.collection.List;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Locale;

import static java.util.Objects.requireNonNull;

@Slf4j
@Service
@RequiredArgsConstructor
public class LiveAlertReportMailActionImpl implements LiveAlertReportMailAction {

    private final Environment environment;
    private final MailSender mailSender;
    private final TemplateService templateService;
    private final MessageSource messageSource;

    @Override
    public Try<Void> send(final User user,
                          final List<TweetHateDetection> tweetHateDetections,
                          final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");

        return templateService.buildLiveAlertReportTemplate(user, tweetHateDetections, locale)
                .flatMap(html -> Try.of(() -> Mail.builder()
                        .to(Sets.newHashSet(user.getEmail()))
                        .from(environment.getProperty("spring.mail.username"))
                        .subject(messageSource.getMessage("live-alert-report-mail-subject", null, locale))
                        .body(html)
                        .build())
                        .onFailure(throwable -> log.error("could not create mail", throwable))
                )
                .flatMap(mailSender::send)
                .onFailure(throwable -> log.error("could not create mail", throwable));
    }
}
