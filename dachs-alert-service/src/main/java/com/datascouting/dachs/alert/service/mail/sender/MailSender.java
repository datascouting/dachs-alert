package com.datascouting.dachs.alert.service.mail.sender;

import com.datascouting.dachs.alert.service.mail.model.Mail;
import javaslang.control.Try;

public interface MailSender {

    Try<Void> send(final Mail mail);
}
