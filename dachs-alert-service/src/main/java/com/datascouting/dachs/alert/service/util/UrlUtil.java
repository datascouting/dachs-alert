package com.datascouting.dachs.alert.service.util;

import javaslang.control.Try;

public class UrlUtil {

    public static Try<String> removeProtocol(String url) {
        return Try.of(() -> url
                .replace("http:", "")
                .replace("https:", ""));
    }
}
