package com.datascouting.dachs.alert.service.microservice.detection.api.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DetectionResponse {

    private Long tweetId;
    private double hateSpeechRate;
    private double personalAttackRate;
}
