package com.datascouting.dachs.alert.service.chart.server.rendering;

import com.datascouting.dachs.alert.domain.entity.User;
import javaslang.control.Try;

import java.nio.file.Path;
import java.util.Date;

public interface ChartServerRenderingService {

    Try<Path> createTrendChartImage(User user, Date startDate, Date endDate, Integer intervalMinutes);

    Try<Path> createConversationHealthChartImage(User user, Date startDate, Date endDate);
}
