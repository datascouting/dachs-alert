package com.datascouting.dachs.alert.service.chart.server.rendering.util;

import javaslang.control.Try;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.util.Objects.requireNonNull;

public class FileUtils {

    public static Try<File> createTempHtmlFile() {
        return Try.of(() -> {
            final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy_HH-mm-ss");
            final String format = dateFormat.format(new Date());

            return File.createTempFile(format, ".html");
        });
    }

    public static Try<File> writeToFile(final File file, final String text) {
        requireNonNull(file, "file is null");
        requireNonNull(text, "text is null");

        return Try.of(() -> {
            final FileOutputStream outputStream = new FileOutputStream(file);
            final byte[] strToBytes = text.getBytes();
            outputStream.write(strToBytes);
            outputStream.close();

            return file;
        });
    }

    public static Try<Path> createEmailImagePath(final Path base) {
        requireNonNull(base, "base is null");

        return Try.of(() -> {
            final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyy_HH-mm-ss");
            final String format = dateFormat.format(new Date());

            return Paths.get(base.toString(), "email_" + format + ".png");
        });
    }
}
