package com.datascouting.dachs.alert.service.microservice.detection.api;

import com.datascouting.dachs.alert.service.microservice.detection.api.model.DetectionResponse;
import com.datascouting.dachs.detection.dto.request.DetectRequest;
import com.datascouting.dachs.detection.sdk.client.DetectionSdkClient;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;

@Service
@RequiredArgsConstructor
public class DetectionApiClientImpl implements DetectionApiClient {

    private final DetectionSdkClient detectionSdkClient;

    @Override
    public Try<DetectionResponse> detect(Tweet tweet) {
        return detectionSdkClient.detect(DetectRequest.builder(tweet.getText())
                .languages(new HashSet<>(Collections.singletonList(tweet.getLanguageCode())))
                .build())
                .flatMap(detectionDto -> Option.ofOptional(detectionDto.getLanguageDetections().stream().findFirst()).toTry())
                .map(detection -> DetectionResponse.builder()
                        .hateSpeechRate(detection.getHateSpeechDetection().getProbability())
                        .personalAttackRate(detection.getPersonalAttackDetection().getProbability())
                        .tweetId(tweet.getId())
                        .build());
    }
}
