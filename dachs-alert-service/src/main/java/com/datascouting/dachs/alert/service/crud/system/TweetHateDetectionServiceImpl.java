package com.datascouting.dachs.alert.service.crud.system;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import com.datascouting.dachs.alert.domain.entity.QTweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.domain.repository.TweetHateDetectionRepository;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import javaslang.collection.List;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.intellift.sol.domain.querydsl.repository.QueryDslRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class TweetHateDetectionServiceImpl implements TweetHateDetectionService {

    private final TweetHateDetectionRepository tweetHateDetectionRepository;
    private final DachsAlertConfigurationProperties properties;

    @Override
    public QueryDslRepository<TweetHateDetection, String> getRepository() {
        return tweetHateDetectionRepository;
    }

    @Override
    public Try<List<TweetHateDetection>> findAll(Predicate predicate) {
        return Try.of(() -> Option.of(tweetHateDetectionRepository.findAll(predicate))
                .map(List::ofAll)
                .getOrElse(List::empty));
    }

    @Override
    public Try<List<TweetHateDetection>> findAll(Predicate predicate, Sort sort) {
        return Try.of(() -> Option.of(tweetHateDetectionRepository.findAll(predicate, sort))
                .map(List::ofAll)
                .getOrElse(List::empty));
    }

    @Override
    public Try<List<TweetHateDetection>> findByRetriesLessThanAndPending(final Integer monitorMaxRetries,
                                                                         final Pageable pageable) {
        requireNonNull(monitorMaxRetries, "monitorMaxRetries is null");
        requireNonNull(pageable, "pageable is null");

        return Try.of(() -> {
            final QTweetHateDetection tweetHateDetection = QTweetHateDetection.tweetHateDetection;

            final BooleanExpression predicate = tweetHateDetection.hateDetectionStatus.eq(HateDetectionStatus.PENDING)
                    .and(tweetHateDetection.retries.lt(properties.getMonitorMaxRetries()));

            return Option.of(tweetHateDetectionRepository.findAll(predicate, pageable))
                    .map(List::ofAll)
                    .getOrElse(List::empty);
        });
    }

    @Override
    public Try<List<TweetHateDetection>> findAllByUser(final User user) {
        Objects.requireNonNull(user, "user is null");

        return Try.of(() -> {
            final BooleanExpression predicate = QTweetHateDetection.tweetHateDetection.user.id.eq(user.getId());

            return Option.of(tweetHateDetectionRepository.findAll(predicate))
                    .map(List::ofAll)
                    .getOrElse(List::empty);
        });
    }
}
