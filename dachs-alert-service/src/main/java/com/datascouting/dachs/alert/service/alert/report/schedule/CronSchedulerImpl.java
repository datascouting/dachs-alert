package com.datascouting.dachs.alert.service.alert.report.schedule;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.alert.report.AlertReportTaskService;
import com.datascouting.dachs.alert.service.alert.report.schedule.context.CronServiceContext;
import javaslang.Tuple;
import javaslang.Tuple2;
import javaslang.collection.List;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ScheduledFuture;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class CronSchedulerImpl implements CronScheduler {

    private final ThreadPoolTaskScheduler threadPoolTaskScheduler;
    private final AlertReportTaskService alertReportTaskService;
    private final CronServiceContext cronServiceContext;

    @Override
    public Try<Void> schedule(final User user) {
        requireNonNull(user, "user is null");

        return createCronTrigger(user)
                .flatMap(this::scheduleCronTrigger)
                .flatMap(cronServiceContext::addScheduledFuture);
    }

    @Override
    public Try<Void> schedule(final List<User> users) {
        requireNonNull(users, "users is null");

        return Try.sequence(users.map(this::schedule))
                .map(ignored -> null);
    }

    @Override
    public Try<Void> unschedule(final User user) {
        requireNonNull(user, "user is null");

        return cronServiceContext.removeScheduledFuture(user);
    }

    @Override
    public Try<Void> unschedule(final String userId) {
        requireNonNull(userId, "userId is null");

        return cronServiceContext.removeScheduledFuture(userId);
    }

    private Try<Tuple2<User, CronTrigger>> createCronTrigger(final User user) {
        requireNonNull(user, "user is null");

        return Try.of(() -> Tuple.of(
                user,
                new CronTrigger(extractCronExpression(user))
        ));
    }

    private String extractCronExpression(final User user) {
        requireNonNull(user, "alert is null");

        return Option.of(user.getReportFrequencyCron())
                .getOrElse("0 0 14 */1 * *");
    }

    private Try<Tuple2<User, ScheduledFuture>> scheduleCronTrigger(final Tuple2<User, CronTrigger> userCronTrigger) {
        requireNonNull(userCronTrigger, "userCronTrigger is null");

        final User user = userCronTrigger._1;
        final CronTrigger cronTrigger = userCronTrigger._2;

        return Try.of(() ->
                Tuple.of(
                        user,
                        threadPoolTaskScheduler.schedule(
                                alertReportTaskService.generateTask(user),
                                cronTrigger
                        )
                ));
    }
}
