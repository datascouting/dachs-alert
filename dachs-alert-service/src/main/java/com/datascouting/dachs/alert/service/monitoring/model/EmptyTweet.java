package com.datascouting.dachs.alert.service.monitoring.model;

import org.springframework.social.twitter.api.Tweet;

import java.util.Date;

public class EmptyTweet extends Tweet {

    public EmptyTweet(Long id) {
        super(id, String.valueOf(id), null, null, null, null, null, 0L, null, null);
    }
}
