package com.datascouting.dachs.alert.service.monitoring;

import com.datascouting.dachs.alert.common.constant.HateDetectionStatus;
import com.datascouting.dachs.alert.domain.entity.TweetHateDetection;
import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import com.datascouting.dachs.alert.service.crud.custom.UserConnectionService;
import com.datascouting.dachs.alert.service.crud.system.TweetHateDetectionService;
import com.datascouting.dachs.alert.service.microservice.detection.api.DetectionApiClient;
import com.datascouting.dachs.alert.service.monitoring.model.EmptyTweet;
import com.datascouting.dachs.alert.service.util.TwitterMissingActions;
import javaslang.Value;
import javaslang.collection.List;
import javaslang.collection.Map;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.function.Function;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProcessPendingTweetHateDetectionImpl implements ProcessPendingTweetHateDetection {

    private final TweetHateDetectionService tweetHateDetectionService;
    private final DetectionApiClient detectionApiClient;
    private final UserConnectionService userConnectionService;
    private final DachsAlertConfigurationProperties properties;

    @Override
    @Scheduled(cron = "${dachs.process-pending-tweet-hate-detection-cron}")
    @Transactional
    public void process() {
        tweetHateDetectionService.findByRetriesLessThanAndPending(properties.getMonitorMaxRetries(), new PageRequest(0, 100))
                .onFailure(throwable -> log.error("Could not fetch PENDING tweetHateDetections", throwable))
                .map(tweetHateDetections -> tweetHateDetections.groupBy(TweetHateDetection::getUser))
                .flatMap(userTweetDetectionsMap -> processUserTweetDetections(userTweetDetectionsMap)
                        .onFailure(throwable -> log.error("Could not process tweet detections", throwable)));
    }

    private Try<List<TweetHateDetection>> processUserTweetDetections(final Map<User, List<TweetHateDetection>> userTweetDetectionsMap) {
        requireNonNull(userTweetDetectionsMap, "userTweetDetectionsMap is null");

        if (userTweetDetectionsMap.size() > 0) {
            return userTweetDetectionsMap.toList()
                    .map(userTweetHateDetections -> {
                        final User user = userTweetHateDetections._1;
                        final List<TweetHateDetection> tweetHateDetections = userTweetHateDetections._2;

                        return userConnectionService.getTwitterConnection(user)
                                .onFailure(throwable -> log.error(String.format("Could not get twitter connection for user %s", user.getId()), throwable))
                                .flatMap(twitterConnection -> Try.of(() -> tweetHateDetections.map(TweetHateDetection::getTweetId))
                                        .flatMap(ids -> TwitterMissingActions.statusesLookup(twitterConnection, ids)
                                                .onFailure(throwable -> log.error(String.format("Could not lookup tweet ids %s", String.join(",", ids.map(String::valueOf))), throwable))))
                                .flatMap(tweets -> buildTweetHateDetections(tweets, tweetHateDetections)
                                        .onFailure(throwable -> log.error(String.format("Could build tweetHateDetections for user %s", user.getId()), throwable)))
                                .flatMap(tweetHateDetectionsToPersist -> tweetHateDetectionService.save(tweetHateDetectionsToPersist)
                                        .onFailure(throwable -> log.error(String.format("Could not persist tweetHateDetections for user %s", user.getId()), throwable)));
                    })
                    .toTry()
                    .flatMap(Function.identity());
        } else {
            return Try.success(List.empty());
        }
    }

    private Try<List<TweetHateDetection>> buildTweetHateDetections(final List<Tweet> tweets,
                                                                   final List<TweetHateDetection> tweetHateDetections) {
        requireNonNull(tweets, "tweets is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");

        return fillWithEmptyTweets(tweets, tweetHateDetections)
                .flatMap(foundTweets -> Try.sequence(foundTweets.map(tweet -> buildTweetHateDetection(tweet, tweetHateDetections))))
                .map(Value::toList);
    }

    private Try<List<Tweet>> fillWithEmptyTweets(final List<Tweet> tweets,
                                                 final List<TweetHateDetection> tweetHateDetections) {
        requireNonNull(tweets, "tweets is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");

        return Try.of(() -> {
            final List<Tweet> foundTweets = List.ofAll(tweets);
            final List<Long> ids = foundTweets.map(Tweet::getId);

            return tweetHateDetections.filter(tweetHateDetection -> !ids.contains(tweetHateDetection.getTweetId()))
                    .foldLeft(foundTweets, (fTweets, tweetHateDetection) ->
                            foundTweets.prepend(new EmptyTweet(tweetHateDetection.getTweetId())));
        });
    }

    private Try<TweetHateDetection> buildTweetHateDetection(final Tweet tweet,
                                                            final List<TweetHateDetection> tweetHateDetections) {
        requireNonNull(tweet, "tweet is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");

        return findTweetHateDetectionByTweetId(tweet.getId(), tweetHateDetections)
                .toTry()
                .flatMap(tweetHateDetection -> {
                    final Function<TweetHateDetection, Try<TweetHateDetection>> failed = aTweetHateDetection -> Try.of(() -> {
                        aTweetHateDetection.setHateSpeechRate(null);
                        aTweetHateDetection.setHateSpeechRate(null);
                        aTweetHateDetection.setHateDetectionStatus(HateDetectionStatus.PENDING);
                        aTweetHateDetection.setRetries(aTweetHateDetection.getRetries() + 1);

                        return aTweetHateDetection;
                    });

                    if (tweet instanceof EmptyTweet) {
                        return failed.apply(tweetHateDetection);
                    }

                    return detectionApiClient.detect(tweet)
                            .flatMap(detectionResponse -> Try.of(() -> {
                                tweetHateDetection.setHateSpeechRate(detectionResponse.getHateSpeechRate());
                                tweetHateDetection.setPersonalAttackRate(detectionResponse.getPersonalAttackRate());
                                tweetHateDetection.setHateDetectionStatus(HateDetectionStatus.COMPLETED);

                                return tweetHateDetection;
                            }))
                            .recoverWith(throwable -> failed.apply(tweetHateDetection));
                });
    }

    private Option<TweetHateDetection> findTweetHateDetectionByTweetId(final Long tweetId,
                                                                       final List<TweetHateDetection> tweetHateDetections) {
        requireNonNull(tweetId, "tweetId is null");
        requireNonNull(tweetHateDetections, "tweetHateDetections is null");

        return tweetHateDetections
                .find(tweetHateDetection -> tweetHateDetection.getTweetId().equals(tweetId));
    }
}
