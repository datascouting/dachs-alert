package com.datascouting.dachs.alert.common.constant;

public abstract class DachsAlertApiEndpoints {

    public static final String USERS = "/users";
    public static final String ROLES = "/roles";

    public static final String RPC_CRON_EVALUATION = "/rpc/cron-evaluation";

    public static final String RPC_CHARTS = "/rpc/charts";

    public static final String RPC_OPT_OUT = "/rpc/opt-out";
}
