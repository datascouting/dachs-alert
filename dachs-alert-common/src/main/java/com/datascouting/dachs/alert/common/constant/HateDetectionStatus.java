package com.datascouting.dachs.alert.common.constant;

public enum HateDetectionStatus {
    PENDING,
    COMPLETED
}
