package com.datascouting.dachs.alert.security.jwt;


import com.datascouting.dachs.alert.domain.entity.Role;
import com.datascouting.dachs.alert.security.service.SecurityUserDetailsService;
import com.datascouting.dachs.alert.service.config.DachsAlertConfigurationProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

    private final SecurityUserDetailsService securityUserDetailsService;
    private final String encodedSecretKey;
    private final Long validityInMilliseconds;

    public JwtTokenProvider(final SecurityUserDetailsService securityUserDetailsService,
                            final DachsAlertConfigurationProperties dachsConfigurationProperties) {
        this.securityUserDetailsService = securityUserDetailsService;

        this.encodedSecretKey = Base64.getEncoder().encodeToString(dachsConfigurationProperties.getJwt().getSecretKey().getBytes());
        this.validityInMilliseconds = dachsConfigurationProperties.getJwt().getExpireLength();
    }

    public String createToken(final String userId, final Set<Role> roles) {
        final Claims claims = Jwts.claims().setSubject(userId);
        claims.put("auth", roles.stream()
                .map(s -> new SimpleGrantedAuthority(s.getName()))
                .collect(Collectors.toList()));

        Date now = new Date();
        Date validity = new Date(now.getTime() + validityInMilliseconds);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, encodedSecretKey)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        UserDetails userDetails = securityUserDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public String getUsername(String token) {
        return Jwts.parser()
                .setSigningKey(encodedSecretKey)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser()
                    .setSigningKey(encodedSecretKey)
                    .parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            throw new RuntimeException("Expired or invalid JWT token");
        }
    }
}
