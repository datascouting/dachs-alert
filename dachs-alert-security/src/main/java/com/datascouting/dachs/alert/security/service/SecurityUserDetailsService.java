package com.datascouting.dachs.alert.security.service;

import com.datascouting.dachs.alert.domain.entity.User;
import com.datascouting.dachs.alert.domain.repository.UserRepository;
import com.datascouting.dachs.alert.security.model.SecurityUser;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class SecurityUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public SecurityUser loadUserByUsername(final String id) throws UsernameNotFoundException {
        final Function<User, SecurityUser> userToCustomUserDetails = user -> {
            final SecurityUser userDetails = new SecurityUser();

            BeanUtils.copyProperties(user, userDetails);

            return userDetails;
        };

        return Try.of(() -> userRepository.findById(id))
                .flatMap(userOption -> userOption.toTry(() -> new UsernameNotFoundException(id)))
                .map(userToCustomUserDetails)
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }
}
