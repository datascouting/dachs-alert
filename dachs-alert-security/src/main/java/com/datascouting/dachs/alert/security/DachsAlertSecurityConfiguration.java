package com.datascouting.dachs.alert.security;

import com.datascouting.dachs.alert.domain.DachsAlertDomainConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@Import(DachsAlertDomainConfiguration.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan
@Configuration
public class DachsAlertSecurityConfiguration {

    @Bean
    RoleHierarchy roleHierarchy() {
        final RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();

        roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_CURATOR");

        return roleHierarchy;
    }
}
